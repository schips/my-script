#!/bin/sh

#alias tmux
alias tls="tmux list-sessions"
alias tlw="tmux list-windows"
alias tlk="tmux -2 list-keys"

## new-session 实现：创建会话并attach（因此使用这个就够了）
#(不需要这个)alias tatt="tmux attach-session -t"
alias ts="tmux -2 new-session -t"
