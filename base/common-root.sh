#*  @brief        基础命令，但与需要root权限

#!/bin/sh

# 将 所有权改为 本用户
function su-make-my() {
    sudo chown ${USER} $@ -R
    sudo chgrp ${USER} $@ -R
}

# 将 所有权改为 本用户
function su-make-sb() {
    local nuser="$1"
    local path="$2"
    if [ -z "$nuser" ];then
        echo " user path"
        return 1
    fi
    sudo chown ${nuser} $path -R
    sudo chgrp ${nuser} $path -R
}

# 将 所有权改为 root
function su-make-root() {
    sudo chown root $@ -R
    sudo chgrp root $@ -R
}

# 踢掉来自 w 命令得到的 用户
function su-make-tty-out() {
    sudo pkill -kill -t $@
}

function su-make-hostname() {
    local newhostname=$1
    if [ ! "$newhostname" ]; then
        return 1
    fi
    sudo hostnamectl set-hostname $newhostname
    sudo hostname                 $newhostname
    sudo sh -c "echo $newhostname > /proc/sys/kernel/hostname"
}

# make password
function mk-pswd () {
    # 随机生成一段字符
    local len=$1
    if [ ! "$len" ]; then
        len=16
    fi
    openssl rand -hex  $len | cut -c1-$len
}

# 添加用户到sudo中
function su-make-sudoer-add () {
    local arg_user=$1
    if [ ! "$arg_user" ]; then
        return 1
    fi
    sudo usermod -aG sudo $arg_user
}

# 从sudo中删掉用户
function su-make-sudoer-del () {
    local arg_user=$1
    if [ ! "$arg_user" ]; then
        return 1
    fi
    sudo gpasswd -d $arg_user sudo
}

function grpexists {
	if [ $(getent group $1) ]; then
		echo "group $1 exists."
        return 0
	else
		echo "group $1 does not exist."
        return 255
	fi
}

function su-make-group-add () {
function su-make-group-add-help () {
cat <<EOF
$0 : add user to group
arg1: group
arg2: user
----
e.g. : su-make-group-add sudo xxx
EOF
}
    local arg_grp=$1
    local arg_user=$2
    if [ ! "$arg_user" ]; then
        su-make-group-add-help $0 ; return 1
    fi

    if [ ! $(getent group $arg_grp) ]; then
        su-make-group-add-help $0 ; return 1
    fi
    sudo usermod -aG $arg_grp $arg_user
}

# 从sudo中删掉用户
function su-make-group-del () {
function su-make-group-del-help () {
cat <<EOF
$0 : add user to group
arg1: group
arg2: user
----
e.g. : su-make-group-del sudo xxx
EOF
}
    local arg_grp=$1
    local arg_user=$2
    if [ ! "$arg_user" ]; then
        su-make-group-del-help $0 ; return 1
    fi

    if [ ! $(getent group $arg_grp) ]; then
        su-make-group-del-help $0 ; return 1
    fi
    sudo gpasswd -d $arg_user $arg_grp
}

# 以 sudo 的权限执行指令
function sd () {
    local arg_grp="$@"
    local sudocmd="/tmp/.sudo.$$.cmd"
    (
    cat <<EOF
    sudo $arg_grp
EOF
) > $sudocmd
    source $sudocmd
}

# 找出并杀死占用文件的进程
function su-make-kill-who-open-file ()
{
    # 需要查找的文件
    local check_file="$1"
    # 排除的字段
    local exclude_string="$2"

    if [ -z "$check_file" ]; then
        return
    fi
    local answer=""

    lsof | grep -w $check_file | while read line
    do
        if [ ! -z "$exclude_string" ]; then
            #echo "try skip $exclude_string"
            echo $line | awk 'NR != 1 {print}'  | grep  $exclude_string && continue
            #echo 'not found ok'
            #echo "111 $?"
        fi
        pid=`echo $line | awk '{print $2}'`
        sinfo=`echo $line | awk '{print$1}'`
        echo "Killing $pid : $sinfo"
        echo "    $line"
        cat << EOF
WARN :
   杀死某些进程可能导致会系统死机
   请确保所杀死的进程是自己熟悉的
EOF
        echo "输入 [y] 继续"
        read answer
        if [ "$answer" = 'y' ]; then
            sudo kill -9 $pid
        fi
        echo "\n\n\n"
    done
}
