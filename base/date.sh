#*  @brief        时间、日期函数集合

#!/bin/sh

## 获取当前时间， 格式为纯数字
function cur-date-pure() {
    local DATE=$(date "+%Y%m%d%H%M%S")
    echo $DATE
}
