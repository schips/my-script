#*  @brief        指定前后动作

export _action_before=""
export _action_after=""

function action-set-before () {
    _action_before="$@"
}

function action-get-before () {
    echo $_action_before
}

function action-set-after () {
    _action_after="$@"
}

function action-get-after () {
    echo $_action_after
}

function action-run () {
    local cmd="$@"
    local fie=/tmp/.$$.cmd
    (
cat <<EOF
$_action_before
$cmd
$_action_after
EOF
    ) > $fie
    ## 要求source，以执行拓展命令
    source $fie
    rm $fie
}
