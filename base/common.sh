#*  @brief        基础命令集合
#!/bin/sh

### ip
alias ipa="ip a"
## common
unalias cp 2>/dev/null
unalias mv 2>/dev/null
#alias cp="cp"
#alias mv="mv"
set +C # 允许覆盖写

## Makefile
alias remake="make clean && make"
alias re="remake"
alias mc="make clean"
alias mi="make install"
function mj()
{
    make -j`allCPU` "$@"
}


### tar 的简写， 快速实现解压缩
# tar/untar
function tt ()
{
function _my_tt ()
{
    if [ ! -f "$1" -a ! -d "$1" ]; then
        echo "[$1] not found."
        return 1
    fi

    local file="$1"
    local filename=`basename $1`
    local _TAR_MODE=`echo $filename | grep -E '.tgz|tar.gz|tar.xz|tar.bz'`
    #echo "${_TAR_MODE}"
    if [ "$_TAR_MODE" ]; then
        echo "Untar [${filename}]"
        tar -xf ${file}
    else
        echo "Making [${filename}.tgz]"
        tar -zcf ${filename}.tgz ${file}
    fi
}
    ## 批量执行
    until [ $# -eq 0 ]
    do
        _my_tt "$1"
        shift
    done
}
function zz ()
{
function _my_zz ()
{
    if [ ! -f "$1" -a ! -d "$1" ]; then
        echo "[$1] not found."
        return 1
    fi

    local file="$1"
    local filename=`basename $1`
    local _TAR_MODE=`echo $filename | grep -E 'zip'`
    #echo "${_TAR_MODE}"
    if [ "$_TAR_MODE" ]; then
        echo "Unzip [${filename}]"
        unzip ${file}
    else
        echo "Making [${filename}.zip]"
        zip ${filename}.zip ${file} -r
    fi
}
    ## 批量执行
    until [ $# -eq 0 ]
    do
        _my_zz "$1"
        shift
    done
}

### string
function set_string_cmd () {
    # 统计 单词个数
    # get word count
    # e.g. : wcnt "12345 678 890"
    function wcnt()  { echo "$@" | wc -w;}
}

 #try wget 避免重复下载
function tget () {
    filename=`basename $1`
    echo "Downloading [${filename}]..."
    wget -nc $1
    echo "[OK] Downloaded [${filename}] "
}

# 写脚本的时候的一些提示
function helpsh () {
function helpsh_dir () {
cat <<EOF
----------
判断文件/目录是否存在
if [ ! -d "dir" ]; then
    #目录不存在"
fi

if [ ! -f "file" ]; then
    #文件不存在
fi
EOF
}

function helpsh_string () {
cat <<EOF
----------
字符串比较
if [ "str1" = "str2" ]; then
    #same
fi

if [ ! "str1" = "str2" ]; then
    #diff
fi

if [ -z "str1" ]; then
    #empty
fi
EOF
}

function helpsh_num () {
cat <<EOF
----------
数字比较
if [ num1 -eq num2 ]; then
    same
fi

if [ num1 -ne num2 ]; then
    diff
fi

if [ num1 -gt num2 ]; then
    >
fi

if [ num1 -ge num2 ]; then
    >=
fi

if [ num1 -lt num2 ]; then
    <
fi

if [ num1 -le num2 ]; then
    <=
fi

----------
数学运算
local i
i=\$((\$i+1)) # 自增
EOF
}

function helpsh_rw () {
cat <<EOF
----------
按行读取
cat \$FILE_LIST | while read line
do
    \$line ..
done
EOF
}

function helpsh_logic () {
cat <<EOF
----------
并列条件 -a ； 或者条件 -o
if [ xx = xx -a yy = yy ]; then
    -a 同时满足才为真
fi

EOF
}
function helpsh_date () {
cat <<EOF
获取当前时间: 20210306112247
local DATE=\$(date "+%Y%m%d%H%M%S")
EOF
}
    local which_part=$1
    if [ ! -z "$which_part" ]; then
        `echo helpsh_$which_part`
        if [ "$which_part" = "help" ]; then
cat <<EOF
dir : 文件目录判断有关
string : 文本有关
num : 数字、运算
rw : 文件读写
date: 日期获取
EOF
        fi
    else
        helpsh_dir
        helpsh_string
        helpsh_num
        helpsh_rw
        helpsh_date
        helpsh_logic
    fi
}

## xshell
bindkey '\e[1~' beginning-of-line 2>/dev/null
bindkey '\e[4~' end-of-line       2>/dev/null


# 快速跳转指定目录 ： here/there
export BASE_HERE=""
## 指定当前路径
function here () {
	BASE_HERE=`pwd`
}
## 跳转到指定的路径
function there () {
	if [ ! -d "$BASE_HERE" ]; then
		return -1
	fi
	cd $BASE_HERE
}

# 创建随机数，可用于量产时创建IP等功能
function rand(){
    local min=$1
    local max=$(($2-$min+1))
    local num=$(($RANDOM+1000000000)) #增加一个10位的数再求余
    if [ -z "$min" -o -z "$max" ]; then
        echo "rand min max"
        return
    fi
    echo $(($num%$max+$min))
#rnd=$(rand 200 210)
#echo $rnd
}

