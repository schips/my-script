#!/bin/sh
#*  @brief        环境有关的

env-path-dump() {
    echo "PATH :"
    echo "----------------"
    echo $PATH | sed 's/:/\n/g' | awk '!x[$0]++'
    echo "----------------"
}

env-ldpath-dump() {
    echo "LD_LIBRARY_PATH :"
    echo "----------------"
    echo $LD_LIBRARY_PATH | sed 's/:/\n/g' | awk '!x[$0]++'
    echo "----------------"
}

# 安全快速添加 Path路径
env-path-add() {
    ## 同于不同路径的两个同名可执行文件的加载顺序问题
    # 系统搜索命令是按环境变量顺序搜索的，搜到了就使用这个命令不往后搜了
    # 如果有一个路径下存在了xx命令，想用新路径下的命令去覆盖它，那么保证新路径先搜索即可。
    if [ ! -d "$1" ]; then
        return
    fi
    local sub=`cd $1; pwd`
    [ -d $sub ] && PATH="$sub:$PATH"
    #echo "add [$sub] to \$PATH"
    #env-path-dump
}

# 安全快速添加 LD_LIBRARY_PATH 路径 库链接路径
# （不同于PATH，链接路径是放在最后去查找，防止干扰正常程序）
env-ldpath-add() {
    ## 同于不同路径的两个同名可执行文件的加载顺序问题
    if [ ! -d "$1" ]; then
        return
    fi
    local sub=`cd $1; pwd`
    [ -d $sub ] || return
    if [ -z "$LD_LIBRARY_PATH" ];then
        LD_LIBRARY_PATH="$sub"
    else
        LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$sub"
    fi
    #echo "add [$sub] to \$LD_LIBRARY_PATH"
    #env-ldpath-dump
}

# get all cpu in this host
function allCPU () {
    local cnt=`grep -c ^processor /proc/cpuinfo 2>/dev/null`
    if [ -z "$cnt" ]; then
		echo 1
    else
		echo $cnt
	fi
}

