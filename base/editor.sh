### vim
_myEditor=`mysc-cfg-get-editor`
# nvim, Neovim
# sudo apt-get install -y neovim

alias edit-todo="${_myEditor} ~/.todo.md && cat ~/.todo.md"

function edit-tmp() {
    local tmpfile=`tmp-gen-safe-file`
    ${_myEditor} $tmpfile
    cat $tmpfile
    rm $tmpfile
}

alias vimdiff="${_myEditor} -d"
alias vc="${_myEditor} ${HOME}/.vimrc"
alias cfg="${_myEditor} ${MYSC_CONFIG}"

#alias vi="${_myEditor}"
function vi ()
{
    ${_myEditor} "$@"
}
function vim ()
{
    ${_myEditor} "$@"
}
alias vd="${_myEditor} -d"

