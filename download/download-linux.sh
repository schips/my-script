#*  @brief        针对Linux下载的脚本

#!/bin/sh

export LINUX_DL_LINK=https://mirrors.edge.kernel.org/pub/linux/kernel

## 下载Linux 2.6
function download-linux26 () {
    local VERSION="2"
    local PATCHLEVEL="6"
    local SUBLEVEL="$1"
    tget ${LINUX_DL_LINK}/v2.6/linux-${VERSION}.${PATCHLEVEL}.${SUBLEVEL}.tar.xz
}

### 下载Linux 3.0
#function download-linux30 () {
#    local VERSION="3"
#    local PATCHLEVEL="0"
#    local SUBLEVEL="$1"
#    tget ${LINUX_DL_LINK}/v3.0/linux-${VERSION}.${PATCHLEVEL}.${SUBLEVEL}.tar.xz
#}

## 下载Linux 3.x、 4.x、 5.x
function download-linux345x () {
    local VERSION="$1"
    local PATCHLEVEL="$2"
    local SUBLEVEL="$3"
    tget ${LINUX_DL_LINK}/v${VERSION}.x/linux-${VERSION}.${PATCHLEVEL}.${SUBLEVEL}.tar.xz
}
