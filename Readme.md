# My-script

Plug-in command for shell, support bash and zsh.

## Background

As linux user, many people have personal shell command in `.bashrc`, me too.

But not all command can work well, so I need to edit my `.xxrc` occasionally.

As `rc` file getting bigger, I need a efficiently workflow for my command, therefor I wrote this framework carried componentized shell command.

## Feature

1. compatible `zsh`、`bash`.

2. easy expansion, commands in `my-script` or `shell env` can refer each other.

3. tiny and fast.

## How to use

1. Get it by `git`

```bash
git clone https://github.com/Schips-Wong/my-script.git   ${HOME}/.my-script
```

2. add following command in your `rc`, such as `.bash.rc` or `.zshrc`.

```bash
source ${HOME}/.my-script/my-script.sh
```

3. After re-login, you will see : `Using [myScript]`

For now, you can use `my-script`.

## How to add new plug-in file

>  e.g. : `foo.sh`

1. Get into `my-script` local dir.

```bash
cd ${HOME}/.my-script/my-script.sh
# or run `mysc`(mysc-root)
```

2. Add `.sh` file in anywhere (but not in `.todo` or `.ref` dir).

```bash
# cat ${HOME}/.my-script/foo.sh
function foo () {
    echo "foo"
}
```

3. Run `reload`(`mysc-reload`) or re-login.

4. Run `foo`, then you will see `foo` in your termial.

## Build-in config

`my-script` has build-in configs, you can see document for more detail.(TODO)
