sudo apt-get install -y libx11-dev:i386 libreadline6-dev:i386 libgl1-mesa-dev g++-multilib
sudo apt-get install -y git flex bison gperf build-essential libncurses5-dev:i386 libssl-dev
sudo apt-get install -y dpkg-dev libsdl1.2-dev libesd0-dev
sudo apt-get install -y git-core gnupg flex bison gperf build-essential
sudo apt-get install -y zip curl zlib1g-dev gcc-multilib g++-multilib
sudo apt-get install -y libc6-dev-i386
sudo apt-get install -y lib32ncurses5-dev x11proto-core-dev libx11-dev
sudo apt-get install -y libgl1-mesa-dev libxml2-utils xsltproc unzip m4
sudo apt-get install -y lib32z-dev ccache
sudo apt-get install -y make make-guile
sudo apt-get install -y tofrodos
sudo apt-get install -y python-markdown libxml2-utils xsltproc zlib1g-dev:i386
sudo apt-get install -y cmake tree ctags openjdk-8-jdk python2.7-dev python-pip

sudo apt-get install -y perlbrew libtemplate-perl libswitch-perl 
