

## 求目录a到目录b的相对路径
function path-get-relative-path ()
{
    local patha="$1"
    local pathb="$2"
    if [ ! -d "$patha" -o ! -d "$pathb" ]; then
        echo "$0  path-a  path-b"
        return 1
    fi
	local pathaa=`cd $patha;pwd`
	local pathba=`cd $pathb;pwd`
    #local oldt=`echo $pathaa| sed 's:\/:\\\/:g'`
    #local newt=`echo $pathba| sed 's:\/:\\\/:g'`
(
cat <<EOF
echo $pathaa | sed 's#$pathba#$pathaa#g'
EOF
) > /tmp/.file.sh
    source /tmp/.file.sh
    rm /tmp/.file.sh

}
