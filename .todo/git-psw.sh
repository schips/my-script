# git-common.sh
function git-keep-psw-15mins () {
    local gname=`git config --global user.name`
    git config --local user.name $gname
    git config credential.helper 'cache --timeout=3600'
}

function git-keep-psw-forever () {
    git config --global credential.helper store
    #git config --global credential.helper osxkeychain
}
