#/* @file         gitAddBig.sh
#*  @brief        用于提交大量数量的文件（例如刚开始创建仓库的时候）
#*  @author       Schips
#*  @date         2020-12-08 09:25:49
#*  @version      v1.0
#*  @copyright    Copyright By Schips, All Rights Reserved
#*
#**********************************************************
#*
#*  @par 修改日志:
#*  <table>
#*  <tr><th>Date       <th>Version   <th>Author    <th>Description
#*  <tr><td>2020-12-08 <td>1.0       <td>Schips    <td>创建初始版本
#*  </table>
#*
#**********************************************************
#*/

#!/bin/sh
###################################################################
function gitAddBig () {
# 最大同时上传文件的数量
MAX_COMMIT_FILE_CNT=8000
# 设置缓冲区大小(5000 MB)
GBUFFERSZ=5242880000

# 用于拷贝 commit 的hook、提交信息等
## gerrit账户
GUSER=schips # 例如 schips
GEMAIL=schips@demo.com

## 服务器的地址
IP_ADD=192.168.1.1

###################################################################
## 不需要改动
TMPLIST=/tmp/filelist.raw
ADDLIST=/tmp/add_list

COUNT=1

function gcommit () {
    git commit -m "Init add"
    git commit --amend --no-edit
    git push
}

function TRY_ADD () {
    echo "$1" >> $ADDLIST
    echo "$1"

    if [ $COUNT -ge $MAX_COMMIT_FILE_CNT ]; then
        COUNT=1
        git add $1
        gcommit
    else
        git add $1 
        COUNT=$(($COUNT+1))
    fi
}

# 防止遗漏
function fix_othres () {
    local FILE_LIST=${TMPLIST}_other_raw
    rm ${FILE_LIST} -f 2>/dev/null
    git status -s > ${FILE_LIST}

    cat $FILE_LIST | while read line
    do
        local file=`echo $line | awk '{printf$2}'`
        git add $file
    done
    gcommit
}

# 分批添加以及提交所有的文件
function gadd_all ()   {
    # 获取文件
    ## 如果没有指定文件列表，那么使用默认生成的文件
    local FILE_LIST=$1
    if [ -z "$FILE_LIST" ]; then
        FILE_LIST=TMPLIST
		rm ${FILE_LIST} -f 2>/dev/null
		find . | grep -v ^./.git/ > ${FILE_LIST}
	else
		if [ ! -f "$FILE_LIST" ]; then
            echo "File list not found."
		fi
	fi
    ## 设置提交信息
    git config --local user.name  $GUSER
    git config --local user.email $GEMAIL
    ## 设置缓冲区大小
    git config --local http.postBuffer ${GBUFFERSZ}

    ## gerrit require
    local GIT_TOP=`git rev-parse --git-dir`
    git config remote.origin.push refs/heads/*:refs/for/*
    scp -P 29418 -p ${GUSER}@${IP_ADD}:/hooks/commit-msg ${GIT_TOP}/hooks/

    for file in `cat $FILE_LIST`
    do
        ## 跳过目录本身
        if test -d $file ; then
            continue
        else
            TRY_ADD $file
        fi
    done
    fix_othres
}

}
