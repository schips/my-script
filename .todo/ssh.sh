#*  @brief        ssh 操作有关的

#!/bin/sh

#ssh
function set_ssh_cmd () {

    # remote host-n : 指定一个ip 以登录该网段的设备
    function rn()  { ssh ${XHOST}@${SNET}.$@ ;}

    # remote root : 指定一个ip 以登录该网段的设备
    function rr()   { ssh root@${SNET}.$@ ;}
    # remote host : 登录到指定的设备上
    function rh()   { ssh ${XHOST}@${SNET}.${SIP} ;}
    # 推送接收 指定设备的文件
    function rpull() { scp ${XHOST}@${HOST_IP}:/home/${XHOST}/$@ . ;}
    function rpush() { scp $@ ${XHOST}@${HOST_IP}:/home/${XHOST}/ ;}
    function rxxpull()   {
        local __FILELIST=`echo $@ | sed 's/ /,/g'`
        scp $@ ${XHOST}@${HOST_IP}:/home/${XHOST}/\{${__FILELIST}\} .
    }

    function ssh_free_su () {
        local ahost=$1
        local aip=$2
        if [ ! -f "${HOME}/.ssh/id_rsa" ]; then
            #make_ssh_key
            echo "TODO"
        fi
        ssh-copy-id -i ~/.ssh/id_rsa.pub ${XHOST}@${HOST_IP}
    }
}

# source时自动执行
ssh_init () {
    if [ ! -z "$1" ]; then
        echo Init Command set for ssh
    fi
    # 获取网络地址
    SNET=`mysc-cfg-get snet`
    # 获取网络主机
    SIP=`mysc-cfg-get sip`
    # 获取主机名
    XHOST=`mysc-cfg-get host`

    HOST_IP=${SNET}.${SIP}
    if [ -z "${HOST_IP}" ]; then
        HOST_IP=localhost
    fi

    if [ -z "${XHOST}" ]; then
        XHOST=${USER}
    fi
    set_ssh_cmd
}
#ssh_init
