#/* @file         get_file_list_from_commit_log.sh
#*  @brief        git提交信息有关的
#*  @details      详细说明
#*  @author       Schips
#*  @date         2020-12-07 16:20:07
#*  @version      v1.0
#*  @copyright    Copyright By Schips, All Rights Reserved
#*/

#!/bin/sh

## 从提交信息中，获取有关的提交清单
function make_file_list_accroding_to_the_git-log () {
    local INFO=$1
    local OUTPUT=file_list_about_${INFO}
    local TMP_GID_LIST=/tmp/.gitcomm_list_about_${INFO}
    local TMP_FILE_LIST=/tmp/.git_file_list_about_${INFO}
    local COMMIT_INFO_DIR=/tmp/commit.info
    git log --pretty=format:"%h %an,%ae %cn,%ce %s"| grep -i $INFO | awk '{print $1}' > $TMP_GID_LIST

    rm $TMP_FILE_LIST 2>/dev/null -rf
    mkdir -p ${COMMIT_INFO_DIR}
    ## 获取每次提交对应的文件
    for gid in `cat $TMP_GID_LIST`; do
        ## 调试 行数
        local line=`git log  ${gid} --pretty=format:"" --name-only -1 | sed -n '$='`
        ## 有些提交，例如合并是没有文件变更的
        if [ -z "$line" ]; then
            echo "$gid have 0 line(s)."
            continue
        fi
        echo "$gid have $line line(s)."
        ## 太多的提交可能是 无关的
        if [ $line -gt 200 ]; then
            echo "--- It maybe hard to handle by manual, skip auto."
            continue
        fi
        git log  ${gid} --pretty=format:"" --name-only -1 >> $TMP_FILE_LIST
        ## 保存提交信息
        git log  ${gid} --pretty=format:"%h %an,%ae %cn,%ce %s" --name-only -1  > ${COMMIT_INFO_DIR}/$gid.commit.info
        echo ""  >> ${COMMIT_INFO_DIR}/$gid.commit.info
        #git log  ${gid} --oneline --name-only -1 | sed -e 1d >> $TMP_FILE_LIST
    done
    if [  -f "${TMP_FILE_LIST}" ]; then
		## 删除 空行
		## 排序，去重
		sed -i '/^\s*$/d' $TMP_FILE_LIST
		cat ${TMP_FILE_LIST} | sort | uniq > ${OUTPUT}
		rm $TMP_FILE_LIST 2>/dev/null -rf
		rm $TMP_GID_LIST  2>/dev/null -rf
		echo "The Commit information had been made in  ${COMMIT_INFO_DIR}"
	fi

}

# usage :
# make_file_list_accroding_to_the_git-log Geass
