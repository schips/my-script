
## sshd 配置 登录端口PORT
function sshd-config-change-port () {
    local port=$1
    ## 修改端口号
    if [ -z "$port" ]; then
        echo "need a port for sshd"
        return 1
    fi
    tmp_file=/tmp/$$USER.$RANDOM
    cp /etc/ssh/sshd_config $tmp_file
    (
    cat <<EOF
    sed -r -i "/Port / s/.*/Port $port/1"   $tmp_file
EOF
    ) > /tmp/.tmp.sshd.sh
    source /tmp/.tmp.sshd.sh
    rm /tmp/.tmp.sshd.sh -f
    sudo cp  $tmp_file /etc/ssh/sshd_config || echo "Try Update Config with [cp $tmp_file /etc/ssh/sshd_config]"
}

## sshd 配置 key 登录（no禁用，否则启动）
function sshd-config-pubkeylogin () {
    local enable=$1

    tmp_file=/tmp/$$USER.$RANDOM
    cp /etc/ssh/sshd_config $tmp_file

    sed -r -i '/RSAAuthentication/d'    $tmp_file
    sed -r -i '/PubkeyAuthentication/d' $tmp_file
    sed -r -i '/密钥/d' $tmp_file
    if [ ! "$enable"  = "no" ]; then
        echo '## 启用密钥登录'          >> $tmp_file
        echo 'RSAAuthentication yes'    >> $tmp_file
        echo 'PubkeyAuthentication yes' >> $tmp_file
    else
        echo '## 禁用密钥登录'          >> $tmp_file
        echo 'RSAAuthentication no'     >> $tmp_file
        echo 'PubkeyAuthentication no'  >> $tmp_file
    fi
    sudo cp  $tmp_file /etc/ssh/sshd_config || echo "Try Update Config with [cp $tmp_file /etc/ssh/sshd_config]"
}

## sshd 配置 密码 登录（no禁用，否则启动）
function sshd-config-passwdlogin () {
    local enable=$1
    tmp_file=/tmp/$$USER.$RANDOM
    cp /etc/ssh/sshd_config $tmp_file
    ## 禁用密码登录的快速脚本（需要先添加对应的密钥: cat id_rsa.pub >> $HOME/.ssh/authorized_keys）
    sed -r -i '/PasswordAuthentication/d' $tmp_file
    sed -r -i '/密码登录/d'               $tmp_file
    if [ ! "$enable"  = "no" ]; then
        echo '## 允许密码登录' >> /etc/ssh/sshd_config"
        echo 'PasswordAuthentication yes' >> /etc/ssh/sshd_config"
        echo "运行密码登录，建议配合fail2ban进行设置"
    else
        echo '## 禁止密码登录'           >> $tmp_file
        echo 'PasswordAuthentication no' >> $tmp_file
cat <<EOF
禁用密码登录，请确认是否添加对应的受信公钥
 确认：cat $HOME/.ssh/authorized_keys
 添加与使用：
    1、生成密钥 "ssh-keygen -t rsa"
    2、cat key.pub >> $HOME/.ssh/authorized_keys
    2、通过ssh 登录时，请指定 -i key 参数

EOF
        cat $HOME/.ssh/authorized_keys
    fi
    sudo cp  $tmp_file /etc/ssh/sshd_config || echo "Try Update Config as [root] with [cp $tmp_file /etc/ssh/sshd_config]"
}

## sshd 配置 key 登录（yes启动，否则禁用）
function sshd-config-rootlogin () {
    local enable=$1
    tmp_file=/tmp/$$USER.$RANDOM
    cp /etc/ssh/sshd_config $tmp_file

    sed -r -i '/PermitRootLogin/d'      $tmp_file
    sed -r -i '/特权登录/d'             $tmp_file
    if [ ! "$enable"  = "yes" ]; then
        echo '## 禁用特权登录'           >> $tmp_file
        echo 'PermitRootLogin no'        >> $tmp_file
    else
        echo "特权登录已经被启动了，这是一个危险的操作"
        echo '## 启用特权登录'           >> $tmp_file
        echo 'PermitRootLogin yes'       >> $tmp_file
    fi
    sudo cp  $tmp_file /etc/ssh/sshd_config || echo "Try Update Config as [root] with [cp $tmp_file /etc/ssh/sshd_config]"
}


## sshd 配置 长时间不操作时不断开
function sshd-config-keep-long-login () {
    local enable=$1
    tmp_file=/tmp/$$USER.$RANDOM
    cp /etc/ssh/sshd_config $tmp_file

    sed -r -i '/ClientAliveInterval/d'      $tmp_file
    sed -r -i '/ClientAliveCountMax/d'      $tmp_file
    sed -r -i '/长时间连接时/d'             $tmp_file
    if [ ! "$enable"  = "yes" ]; then
        echo '## 长时间连接时断开'          >> $tmp_file
        echo 'ClientAliveInterval 0'        >> $tmp_file
        echo 'ClientAliveCountMax 3'        >> $tmp_file
    else
        # 表示每60秒连接一下客户端，最大重试次数为5次。
        echo '## 长时间连接时不断开'        >> $tmp_file
        echo 'ClientAliveInterval 60'       >> $tmp_file
        echo 'ClientAliveCountMax 5'        >> $tmp_file
    fi
    sudo cp  $tmp_file /etc/ssh/sshd_config || echo "Try Update Config as [root] with [cp $tmp_file /etc/ssh/sshd_config]"
}
