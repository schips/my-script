# my-script

一个可拓展的shell脚本集，附带了在日常生活中常见的命令集。

## 背景

熟悉Linux命令的人肯定会有自己的一套命令集合。但是如果把命令都写在一个文件中，会导致维护和添加比较混乱，`my-script`由此应运而生。

同时，为了更好地管理脚本，`my-script`内置了对应的命令。

## 特性

1、兼容性良好：支持 zsh、bash。

2、拓展简单：脚本的删除与添加都非常方便

3、允许快速修改命令。

## 用法

### 初始化

```bash
$ git clone https://gitee.com/schips/my-script.git   ${HOME}/.my-script
$ source ~/.my-script/my-script.sh
```

### 增减脚本

作为使用者，你只需要将自己的脚本，放在`my-script.sh`下的任意目录中，在重新`source ${PATH_FOR_MYSC}/my-script.sh`以后或者使用`mysc-reload`即可生效。

> 以`PATH_FOR_MYSC`代指脚本所在目录，下同。

例如：

```bash
$ cat dir.sh
# 备份
function bak () {
    cp $1 $1.bak -rv
}
$ cp dir.sh   ${PATH_FOR_MYSC}/xxx/xxx.sh
$ mysc-reload
```

不想使用时，删除对应的脚本文件即可。

### 修改配置

配置是作为my-script中的一个关键部分，通过`mysc-cfg-set`与`mysc-cfg-get`分别进行设置与获取。

```
$ mysc-cfg-set item [value]
$ mysc-cfg-get item
```

例如：

```bash
## 设置
$ mysc-cfg-set editor vim
## 获取
$ var=`mysc-cfg-get editor`
$ echo $var
```

#### 内置变量

| 类别   | 变量名              | 意义                                        |
| ------ | ------------------- | ------------------------------------------- |
| 编辑器 | editor              | my-script的默认编辑器（推荐用vim）          |
| 编辑器 | editor_can_jmp_line | my-script的默认编辑器配置，用于定位目标行数 |
| git    | author              | my-script的指定作者，常用于git              |
| git    | email               | 类似于author                                |
| net    | snet                | 网络地址，用于网络有关的命令                |
| net    | spi                 | 主机地址                                    |
| net    | host                | 主机名                                      |

### 修改命令

my-script支持快速修改命令。她能够快速找到命令所在的文件，并通过指定的编辑器进行编辑定位。

例如：

```bash
$ mysc-cfg-set editor vim
$ mysc-cfg-set editor_can_jmp_line 1

$ mysc-changeCmd bak
```

## 最后

希望大家也能分享自己所用的命令，也欢迎各种issue轰炸，谢谢大家的支持。
