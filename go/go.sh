#!/bin/sh

#For Golang ( arm-go 要和arm-gcc捆绑在一起)
function set_go_path () {
    #if [ $ARMLINUXGCC = "ARM*" ]
    #then
    #    GO_CC_FOR_TARGET="$ARM_GCC_PATH/arm-linux-gnueabi-gcc"
    #    GO_CXX_FOR_TARGET="$ARM_GCC_PATH/arm-linux-gnueabi-g++"
    #fi
    export GOROOT_BOOTSTRAP=/usr/local/go-go1.4.2
    #export CC_FOR_TARGET=$GO_CC_FOR_TARGET
    #export CXX_FOR_TARGET=$GO_CXX_FOR_TARGET
    export GOROOT=/usr/local/go
    export GOPATH=$HOME/.go
    export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
    #alias arm-go="GOOS=linux GOARCH=arm GOARM=7 go build"
    alias gob="go build"
}
set_go_path
