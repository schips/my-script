## 搜索 c/c++ 函数，尽量过滤掉那些明显就是调用的使用情况
function cfun () {
    # 因为 通过宏定义的'函数' 一般都会有 反斜杠结尾，因此不是很好处理，只能是分成2步
    (
    fw "$*" | grep  "define" | grep "$*"
    ) || (
    fw "$*" | grep -v ";" | grep -v "=" | grep -v "return" | grep -v "\->" |  grep -v  "\." -F  | grep -v "%" | grep -v "()"| grep -v "/*" -F| grep -v "*/" -F| grep "$*"
    )
}

## 搜索 c/c++ 结构体，尽量过滤掉那些明显就是调用的使用情况
function cst () {
    local struct="$*"
    (
    # 存在typedef的写法，不可以单纯地 `grep -v ";"`
    fw "$struct" | grep  -v "struct" | grep  "${struct};$" |  grep -v "("| grep -v "="  |  grep -v "static" | grep -v "const"| grep -v ")" | grep -v "*" -F | grep -v "[" -F| grep -v "]" -F | grep -v "return" | grep "$struct"
    ) || (
    #struct xx 的写法
    fw "struct $struct" | grep -v ";" |  grep -v "("| grep -v "="  |  grep -v "static" | grep -v "const"| grep -v ")" | grep -v "*" -F | grep -v "[" -F| grep -v "]" -F |grep -v "return"  |  grep  -v "${struct};$" | grep "$struct"
    )
}
