## gcc有关的环境脚本

## 检查程序所需的依赖库
function gcc-check-lib-needed ()
{
    if [ ! -z "$1" ]; then
        objdump -p "$@" | grep NEEDED | sort | uniq
    fi
}
