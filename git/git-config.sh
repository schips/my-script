#*  @brief        Git 有关配置
#!/bin/sh

export GIT_DEFALUT_USER_NAME=schips
export GIT_DEFALUT_USER_EMAIL=schips@dingtalk.com

# 是否隐藏zsh访问 git 仓库时自动查询git-status
function git-config-zsh-hide-git-status() {
    local hide="$1"
    if [ -z $hide ];then
        echo "yes/hide, no/show"
    fi
    case $hide in
        Y*|y*|h* )
            git config --add oh-my-zsh.hide-status 1
            echo "hide git-status when zsh staying a git-repo"
            ;;
        N*|n*|s* )
            git config --add oh-my-zsh.hide-status 0
            echo "show git-status when zsh staying a git-repo"
            ;;
    esac
}

## 禁用八进制引用，以解决中文乱码问题
function git-config-disable-quotepath () {
    cat <<EOF
Disable quotepath for utf8 file name
Then, you need to change your Terminal.
    e.g. : Options->Text-> Character set : UTF-8
EOF

    git config --global core.quotepath false
}

## 忽略换行符导致的diff差异
function git-config-ignore-diff-crlf () {
    echo "[全局]配置忽略换行符不同导致的差异"
    git config --global core.whitespace cr-at-eol
    cat <<EOF
    根据系统环境使用下列的命令
      Linux : git-config-crlf-linux
      Windos: git-config-crlf-windos
EOF
}

## 配置用户名以及邮箱
function git-config-name-email-local () {
    local name="$1"
    local mail="$2"
    ## 两者都为空，代表查询
    if [ -z "$name" -a -z "$mail" ]; then
        git config --local user.name
        git config --local user.email
        return 0
    fi
    ## 两者都有值，代表设置
    if [ -z "$name"  -o -z "$mail"  ]; then
        echo "need : name email"
        return 1
    fi
    echo "[本地]配置用户名 & 邮箱"
    echo "git config --local user.name  ${name}"
	git config --local user.name  ${name}
    echo "git config --local user.email  ${mail}"
	git config --local user.email ${mail}
}

function git-config-name-email-global () {
    local name="$1"
    local mail="$2"
    ## 两者都为空，代表查询
    if [ -z "$name" -a -z "$mail" ]; then
        git config --global user.name
        git config --global user.email
        return 0
    fi
    ## 两者都有值，代表设置
    if [ -z "$name"  -o -z "$mail"  ]; then
        echo "need : name email"
        return 1
    fi
    echo "[全局]配置用户名 & 邮箱"
    echo "git config --global user.name  ${name}"
	git config --global user.name  ${name}
    echo "git config --global user.email ${mail}"
	git config --global user.email ${mail}
}

# 针对gerrit的push审核配置
function git-gerrit-config ()
{
    echo "config[remote.origin.push 'refs/heads/*:refs/for/*'] when git-push to gerrit"
    bash -v <<EOF
git config remote.origin.push 'refs/heads/*:refs/for/*'
EOF
}
function git-config-gerrit-push-remote ()
{
    git-gerrit-config
}

# 允许长路径(4096)
function git-config-long-path()
{
    echo "[全局]配置长路径支持(4096)"
    git config --global core.longpaths true
}


# 是否忽略文件模式(Chmod)更改，不带参数执行时默认忽略
# local 默认是不忽略的，而global 会被local覆盖
function git-config-file-mode-local ()
{
    if [ -z "$1"  ]; then
        echo "Disable filemode trace."
        echo " for enable filemode trace, use 'git-config-file-mode-local y'."
        git config       --local core.filemode false && return 0
        git config --add --local core.filemode false
    else
        echo "Enable git filemode trace."
        echo " for disable filemode trace, use 'git-config-file-mode-local'."
        git config       --local core.filemode true && return 0
        git config --add --local core.filemode true
    fi
}
function git-config-editor ()
{
    if [ ! -z "$*" ]; then
        echo "git editor is [$@]"
    fi
    echo "[全局]配置编辑器"
    echo "git config --global --replace-all core.editor $1"
    git config --global --replace-all core.editor $1
}

# 为Linux环境配置文件的换行回车
function git-config-crlf-linux()
{
    git config --global core.autocrlf input
}

# 为Windos环境配置文件的换行回车
function git-config-crlf-windos()
{
    git config --global core.autocrlf true
}

function git-init-config () {
    git-config-editor `mysc-cfg-get editor`
    git-config-ignore-diff-lf-m
    git-config-zsh-hide-git-status hide
    git-config-name-email-global $GIT_DEFALUT_USER_NAME $GIT_DEFALUT_USER_EMAIL
    git-config-disable-quotepath
    git-config-long-path
}
function git-config-init () {
    git-init-config
}

