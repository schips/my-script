#!/bin/sh

# 忘记记录的密码
function git-passwd-forget ()
{
    git config --global --unset credential.helper
}

# 永久记住密码
function git-passwd-keep-forever ()
{
    git config --global credential.helper store
}
# 暂时记住密码
function git-passwd-keep-for-a-while ()
{
    local timeout_s=$1
    local timeout_s_default=3600

    if [ -z "$timeout_s" ]; then
        timeout_s=t$imeout_s_default
    fi
    git config --global credential.helper "cache --timeout=${timeout_s}"
}

