#*  @brief        git有关的修复命令
#!/bin/sh
# fix folling case:
#   unable to access 'https://github.com/.../.git': Could not resolve host: github.com
#   gnutls_handshake() failed: The TLS connection was non-properly terminated.
function git-fix-github ()
{
    cat <<EOF
try fixing folling case:
    unable to access 'https://github.com/.../.git': Could not resolve host: github.com
    gnutls_handshake() failed: The TLS connection was non-properly terminated.
EOF
    git config --global --unset http.proxy
    git config --global --unset https.proxy
}

# 清除无用的提交记录
function git-fix-unuseless-obj ()
{
    local set_timeout=10
    cat <<EOF
Going to remove obj that not in 'git log'.
After [$set_timeout] sec(s), '.git' will be clean.

This action CAN NOT be recovered.
EOF
    for i in `seq ${set_timeout}`
    do

        echo -n "."
        sleep 1
        #sleep $set_timeout
    done
    echo -e "\nFixing unuseless obj"
    git reflog expire --expire=now --all
    git gc --prune=now --aggressive
}

# 将当前的分支覆盖指定分支
## 适用场景
##    在 checkou分支以后做了修改，原先的分支不想要了，提示'HEAD detached from xxx'。
##    此时就可以 git-fix-branch-replace-from-current master
function git-fix-branch-replace-from-current()
{
    local cur_date=$(date "+%Y%m%d%H%M%S")
    local branch="$1"
    local set_timeout=5
    if [ -z "$branch" ]; then
        branch="master"
    fi
    local backup_dir=".git.${cur_date}"

    cat <<EOF
Going to replace branch [$branch].
After [$set_timeout] sec(s), '.git' will be change.
A backup will gen bin  [$backup_dir]

This action CAN NOT be recovered.
EOF
    for i in `seq ${set_timeout}`
    do

        echo -n "."
        sleep 1
        #sleep $set_timeout
    done

    bash <<EOF
    gitROOT=`git rev-parse --show-toplevel 2>/dev/null`
    if [ -z "\$gitROOT"  ]; then
        return 1
    fi
    cd \$gitROOT
    rm .git.$cur_date -rf
    cp .git .git.$cur_date -rf
    git branch -D $branch &&  git checkout -b $branch
EOF
}
