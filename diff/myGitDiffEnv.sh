#/* @file         myGitDiffEnv.sh
#*  @brief        Creat diff env with file list, helpful for push/pull
#*  @author       Schips
#*  @date         2020-11-26 11:30:30
#*  @version      v1.0
#*  @copyright    Copyright By Schips, All Rights Reserved
#*/

#!/bin/sh
function create_with_gitid_help () {
cat <<EOF
create_with_gitid  gdir  gid-1  gid-2 
 - Gdir      : the path of git repository
 - Gid 1     : git commit id 1 for diff
 - Gid 2     : git commit id 2 for diff
 - File list : [Optional] a file list made from 'git diff --name-only'
EOF
return -1
}

DEFINE_INFO_PATH_1=.path_info.1
DEFINE_INFO_PATH_2=.path_info.2
DEFINE_INFO_PATH=.path_info.in_list

function create_with_gitid () {
    local GDIR=$1
    local GID1=$2
    local GID2=$3
    local GLIST=$4

    if [ -z "$1" ]; then
        create_with_gitid_help || return 
    fi
    if [ -z "$2" ]; then
        create_with_gitid_help || return 
    fi
    if [ -z "$3" ]; then
        create_with_gitid_help || return 
    fi

    if [ ! -d "$1" ]; then
        create_with_gitid_help || return 
    fi

    local HERE=`pwd`
    local _DIR1=`cd $DIR1 ; pwd`
    local _DIR2=`cd $DIR2 ; pwd`

    #local TOP_DIR1=`basename ${_DIR1}`
    #local TOP_DIR2=`basename ${_DIR2}`
    local DATE=$(date "+%Y%m%d%H%M%S")
    local TMP_DIR=/tmp/schips_tmp
    #echo $DATE
    mkdir ${HERE}/make_diff_${DATE} -p

    rm ${TMP_DIR} -rf 2>/dev/null
    mkdir ${TMP_DIR} -p

    TOP_DIR=`cd ${HERE}/make_diff_${DATE}; pwd`

    ## 生成 差异列表
	cd ${REPOSITORY}
    if [ -z "$GLIST" ]; then
        git diff $GID1 $GID2 --name-only > $FILE_LIST
        GLIST=$FILE_LIST
    else
        FILE_LIST=$GLIST
    fi

    for file in `cat $FILE_LIST`
    do
        if [ ! -f "${file}" ]; then
            echo ${file} >> ${TOP_DIR}/.diff_but_not_support
            continue
        fi

        # 用于构建目录
        BASE_FILE_NAME=`basename ${file}`
        mkdir  ${TOP_DIR}/${BASE_FILE_NAME} -p

        #准备工作区
        ## 根据补丁获取对应版本的代码
		### 文件1
        echo "fetch 1"
        git show ${GID1}:${file} > ${TMP_DIR}/${BASE_FILE_NAME}
        #local ret=$?
        #if [ ! $ret -ne 0 ]; then
        #    echo "1 not found"
        #    echo ${GID1}:${file} >> ${TOP_DIR}/.diff_but_nofound
        #    continue
        #fi
        cp ${TMP_DIR}/${BASE_FILE_NAME}  ${TOP_DIR}/${BASE_FILE_NAME}/1.${BASE_FILE_NAME}
		### 文件2
        echo "fetch 2"
        git show ${GID2}:${file} > ${TMP_DIR}/${BASE_FILE_NAME}
        #local ret=$?
        #if [ ! $ret -ne 0 ]; then
        #    echo "2 not found"
        #    echo ${GID2}:${file} >> ${TOP_DIR}/.diff_but_nofound
        #    continue
        #fi
        cp ${TMP_DIR}/${BASE_FILE_NAME}  ${TOP_DIR}/${BASE_FILE_NAME}/2.${BASE_FILE_NAME}

        ## 生成差异
        echo "make diff"
        diff ${TOP_DIR}/${BASE_FILE_NAME}/1.${BASE_FILE_NAME} ${TOP_DIR}/${BASE_FILE_NAME}/2.${BASE_FILE_NAME} > ${TOP_DIR}/${BASE_FILE_NAME}/.diff.${BASE_FILE_NAME}
        # 保存信息 以便于 另外的脚本处理
        ## 路径1
        echo ${GID1}:${file} >> ${TOP_DIR}/${BASE_FILE_NAME}/.path_info.1
        ## 路径2
        echo ${GID2}:${file} >> ${TOP_DIR}/${BASE_FILE_NAME}/.path_info.2
        ## 相对路径
        echo ${file} >> ${TOP_DIR}/${BASE_FILE_NAME}/.path_info.in_list
    done
    cd ${HERE}
    echo "Created : [$TOP_DIR]"
}

function pspl_help () {
cat <<EOF
ps1/ps2 , pl1/pl2 : 
  Push/Pull file 1/2
    - Must in the dir where has .path_info
    - And .path_info could not be empty

EOF
return -1
}

function diffall() {
	local FLAG=0
    local BASE=`pwd`
    for dir in `find . -maxdepth 1 -type d`
    do
        if [ "$dir" = "."  ]; then
            continue
        fi
        cd ${BASE}/${dir}
        echo "Vimdiff [$dir] in [$BASE]"
        vimdiff 1* 2*

        # 为了实现不再询问 todo
		#if [ $FLAG -eq 1]; then
        #    if read -t 5 -p "Continue? :\n\t n : Next   s : Stop  k : Keep" answer
        #    then
        #        echo "你输入的网站名是 $website"
        #    else
        #        echo Timeout when vimdiff loop
        #    fi
        #    break
        #fi
    done
    cd $BASE
}

## push file 1.* to DEFINE_INFO_PATH_1 
function ps1 () {
    if [ ! -f ${DEFINE_INFO_PATH_1} ]; then
        pspl_help || return 
    fi

    local FILE_OUTPUT=`cat ${DEFINE_INFO_PATH_1}`
    if [ ! -f ${FILE_OUTPUT} ]; then
        pspl_help || return 
    fi
    # 备份
    echo "Back up"
    cp $FILE_OUTPUT $FILE_OUTPUT.bak -v
    echo "Push file 1"
    cp 1.* $FILE_OUTPUT -v
}

## push file 2.* to DEFINE_INFO_PATH_2
function ps2 () {
    if [ ! -f ${DEFINE_INFO_PATH_2} ]; then
        pspl_help || return 
    fi

    local FILE_OUTPUT=`cat ${DEFINE_INFO_PATH_2}`
    if [ ! -f ${FILE_OUTPUT} ]; then
        pspl_help || return 
    fi
    # 备份
    echo "Back up"
    cp $FILE_OUTPUT $FILE_OUTPUT.bak -v
    echo "Push file 2"
    cp 2.* $FILE_OUTPUT -v
}

## Pull file 1.* from DEFINE_INFO_PATH_1
function pl1 () {
    if [ ! -f ${DEFINE_INFO_PATH_1} ]; then
        pspl_help || return 
    fi

    local FILE_INPUT=`cat ${DEFINE_INFO_PATH_1}`
    if [ ! -f ${FILE_INPUT} ]; then
        pspl_help || return 
    fi
    FIL1=`ls 1.* | head -n 1`
    echo "Back up"
    cp $FIL1 .bak.$FIL1 -v
    echo "Pull file 1"
    cp $FILE_INPUT $FIL1 -v
}

## Pull file 2.* from DEFINE_INFO_PATH_2
function pl2 () {
    if [ ! -f ${DEFINE_INFO_PATH_2} ]; then
        pspl_help || return 
    fi

    FILE_INPUT=`cat ${DEFINE_INFO_PATH_2}`
    if [ ! -f ${FILE_INPUT} ]; then
        pspl_help || return 
    fi
    FIL2=`ls 2.* | head -n 1`
    echo "Back up"
    cp $FIL1 .bak.$FIL2 -v
    echo "Pull file 2"
    cp $FILE_INPUT $FIL2 -v
}

function helpme () {
    create_with_gitid_help
    pspl_help
}


function create_with_gitid_backup () {
    local GDIR=$1
    local GID1=$2
    local GID2=$3

    if [ -z "$1" ]; then
        create_with_gitid_help || return 
    fi
    if [ -z "$2" ]; then
        create_with_gitid_help || return 
    fi
    if [ -z "$3" ]; then
        create_with_gitid_help || return 
    fi

    if [ ! -d "$1" ]; then
        create_with_gitid_help || return 
    fi

    HERE=`pwd`
	REPOSITORY=`cd $GDIR ; pwd`
    local FILE_LIST=/tmp/.gdiff_env_list

    local _DIR1=`cd $DIR1 ; pwd`
    local _DIR2=`cd $DIR2 ; pwd`

    #local TOP_DIR1=`basename ${_DIR1}`
    #local TOP_DIR2=`basename ${_DIR2}`
    local DATE=$(date "+%Y%m%d%H%M%S")
    local TMP_DIR=/tmp/schips_tmp
    #echo $DATE
    mkdir ${HERE}/make_diff_${DATE} -p

    rm ${TMP_DIR} -rf 2>/dev/null
    mkdir ${TMP_DIR} -p

    TOP_DIR=`cd ${HERE}/make_diff_${DATE}; pwd`

    ## 生成 差异列表
	cd ${REPOSITORY}
	git diff $GID1 $GID2 --name-only > $FILE_LIST

    for file in `cat $FILE_LIST`
    do
        if [ ! -f "${file}" ]; then
            echo ${file} >> ${TOP_DIR}/.diff_but_not_support
            continue
        fi

        # 用于构建目录
        BASE_FILE_NAME=`basename ${file}`
        mkdir  ${TOP_DIR}/${BASE_FILE_NAME} -p

        #准备工作区
        ## 根据补丁获取对应版本的代码
		### 文件1
        echo "fetch 1"
        git diff HEAD ${GID1} ${file} > ${TMP_DIR}/for_fetch
        local ret1=$?
        git diff ${GID1} HEAD ${file} > ${TMP_DIR}/for_restore # 反向补丁
        git apply --check  ${TMP_DIR}/for_fetch && git apply  ${TMP_DIR}/for_fetch
        if [ ! -f "${file}" ]; then
            echo "1 not found"
            echo ${GID1}:${file} >> ${TOP_DIR}/.diff_but_nofound
        	git apply --check  ${TMP_DIR}/for_restore && git apply  ${TMP_DIR}/for_restore
        	#cp ${TMP_DIR}/${BASE_FILE_NAME} ${REPOSITORY}/${file}
            continue
        fi
        cp ${REPOSITORY}/${file}  ${TOP_DIR}/${BASE_FILE_NAME}/1.${BASE_FILE_NAME}
        git apply --check  ${TMP_DIR}/for_restore && git apply  ${TMP_DIR}/for_restore
		### 文件2
        echo "fetch 2"
        git diff HEAD ${GID2} ${file} > ${TMP_DIR}/for_fetch
        git diff ${GID2} HEAD ${file} > ${TMP_DIR}/for_restore # 反向补丁
        git apply --check  ${TMP_DIR}/for_fetch && git apply  ${TMP_DIR}/for_fetch
        if [ ! -f "${file}" ]; then
            echo "2 not found"
            echo ${GID2}:${file} >> ${TOP_DIR}/.diff_but_nofound
        	git apply --check  ${TMP_DIR}/for_restore && git apply  ${TMP_DIR}/for_restore
        	#cp ${TMP_DIR}/${BASE_FILE_NAME} ${REPOSITORY}/${file}
            continue
        fi
        cp ${REPOSITORY}/${file}  ${TOP_DIR}/${BASE_FILE_NAME}/2.${BASE_FILE_NAME}
        git apply --check  ${TMP_DIR}/for_restore && git apply  ${TMP_DIR}/for_restore

        ## 生成差异
        echo "make diff"
        diff ${TOP_DIR}/${BASE_FILE_NAME}/1.${BASE_FILE_NAME} ${TOP_DIR}/${BASE_FILE_NAME}/2.${BASE_FILE_NAME} > ${TOP_DIR}/${BASE_FILE_NAME}/.diff.${BASE_FILE_NAME}
        # 保存信息 以便于 另外的脚本处理
        ## 路径1
        echo ${GID1}:${file} >> ${TOP_DIR}/${BASE_FILE_NAME}/.path_info.1
        ## 路径2
        echo ${GID2}:${file} >> ${TOP_DIR}/${BASE_FILE_NAME}/.path_info.2
        ## 相对路径
        echo ${file} >> ${TOP_DIR}/${BASE_FILE_NAME}/.path_info.in_list
    done
    cd ${HERE}
    echo "Created : [$TOP_DIR]"
}

function pspl_help () {
cat <<EOF
ps1/ps2 , pl1/pl2 : 
  Push/Pull file 1/2
    - Must in the dir where has .path_info
    - And .path_info could not be empty

EOF
return -1
}

function diffall() {
	local FLAG=0
    local BASE=`pwd`
    for dir in `find . -maxdepth 1 -type d`
    do
        if [ "$dir" = "."  ]; then
            continue
        fi
        cd ${BASE}/${dir}
        echo "Vimdiff [$dir] in [$BASE]"
        vimdiff 1* 2*

        # 为了实现不再询问 todo
		#if [ $FLAG -eq 1]; then
        #    if read -t 5 -p "Continue? :\n\t n : Next   s : Stop  k : Keep" answer
        #    then
        #        echo "你输入的网站名是 $website"
        #    else
        #        echo Timeout when vimdiff loop
        #    fi
        #    break
        #fi
    done
    cd $BASE
}

## push file 1.* to DEFINE_INFO_PATH_1 
function ps1 () {
    if [ ! -f ${DEFINE_INFO_PATH_1} ]; then
        pspl_help || return 
    fi

    local FILE_OUTPUT=`cat ${DEFINE_INFO_PATH_1}`
    if [ ! -f ${FILE_OUTPUT} ]; then
        pspl_help || return 
    fi
    # 备份
    echo "Back up"
    cp $FILE_OUTPUT $FILE_OUTPUT.bak -v
    echo "Push file 1"
    cp 1.* $FILE_OUTPUT -v
}

## push file 2.* to DEFINE_INFO_PATH_2
function ps2 () {
    if [ ! -f ${DEFINE_INFO_PATH_2} ]; then
        pspl_help || return 
    fi

    local FILE_OUTPUT=`cat ${DEFINE_INFO_PATH_2}`
    if [ ! -f ${FILE_OUTPUT} ]; then
        pspl_help || return 
    fi
    # 备份
    echo "Back up"
    cp $FILE_OUTPUT $FILE_OUTPUT.bak -v
    echo "Push file 2"
    cp 2.* $FILE_OUTPUT -v
}

## Pull file 1.* from DEFINE_INFO_PATH_1
function pl1 () {
    if [ ! -f ${DEFINE_INFO_PATH_1} ]; then
        pspl_help || return 
    fi

    local FILE_INPUT=`cat ${DEFINE_INFO_PATH_1}`
    if [ ! -f ${FILE_INPUT} ]; then
        pspl_help || return 
    fi
    FIL1=`ls 1.* | head -n 1`
    echo "Back up"
    cp $FIL1 .bak.$FIL1 -v
    echo "Pull file 1"
    cp $FILE_INPUT $FIL1 -v
}

## Pull file 2.* from DEFINE_INFO_PATH_2
function pl2 () {
    if [ ! -f ${DEFINE_INFO_PATH_2} ]; then
        pspl_help || return 
    fi

    FILE_INPUT=`cat ${DEFINE_INFO_PATH_2}`
    if [ ! -f ${FILE_INPUT} ]; then
        pspl_help || return 
    fi
    FIL2=`ls 2.* | head -n 1`
    echo "Back up"
    cp $FIL1 .bak.$FIL2 -v
    echo "Pull file 2"
    cp $FILE_INPUT $FIL2 -v
}

function helpme () {
    create_with_gitid_help
    pspl_help
}


