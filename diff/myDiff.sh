#/* @file         myDiff.sh
#*  @brief        对比有关的
#*/

#!/bin/sh

## diff dir
function ddir () {
    if [ -z "$1" ]; then
        return
    fi 

    if [ -z "$2" ]; then
        return
    fi 

    echo "Making file list for $1"
    find $1 -printf "%P\n" | sort > /tmp/.ddir1
    echo "Making file list for $2"
    find $2 -printf "%P\n" | sort > /tmp/.ddir2
    echo "Diff \n ->$1 \n <- $2"
    diff /tmp/.ddir1 /tmp/.ddir2 | tee /tmp/ddir
    rm /tmp/.ddir1 /tmp/.ddir2
}
