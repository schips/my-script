#!/bin/sh
function vf() {
    # string : 'include/linux/irqdesc.h:55: xx'
    local string="$1"
    local file=`echo $string | awk -F: '{print$1}'`
    local line=`echo $string | awk -F: '{print$2}'`
    local cmd=""
    if [ ! -f "$file"  ]; then
        echo "$file not found"
        return 1
    fi
    if [ ! -z "$line"  ]; then
        cmd="+$line"
    fi
    vim $file $cmd
}
