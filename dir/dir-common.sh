#*  @brief        目录、文件操作有关的

#!/bin/sh

# 显示文件 中 第 n 行 ~ 第 m 行
function showline () {
    local __FILE=$1
    local __LINE_START=$2
    local __LINE_END=$3
    local __SUB=$((${__LINE_END}-${__LINE_START}))
    local __LND=$((${__LINE_END}))
    if [ $# -lt 3 ]; then
        echo "$0 line_head line_end"
        return
    fi
    sed -n "$2,$3p" ${__FILE}
}

# dir tree
function dt () {
    tree -aC
}

# dir tree full
function dtf () {
    tree -aCif
}

# cd with file-path
# 切换目录到文件所在目录
function cdf () {
    local file=$1
    if [  -z "$file" ]; then
        cd
        return 0
    fi
    local _fpath=`dirname $file`
    local fpath=`cd $_fpath > /dev/null ; pwd`
    if [ `pwd` = "$fpath" ]; then
        return 0
    fi
    cd $fpath
}
alias cf="cdf"


# 备份
function bak () {
    local taget=`echo ${1%*/}`
    cp $taget $taget.bak -rv
}

# try backup(备份文件已存在时不备份)
function tbak () {
    local taget=`echo ${1%*/}`
    if [ !  -d "$taget.bak"  -a ! -f "$taget.bak"  ]; then
        cp $taget $taget.bak -rv
    else
        return 1
    fi
}

# 强制拷贝(哪怕目录不在)
function cpcp ()
{
	if [ $# -le 1 ]; then
        return
	fi
    # 获取最后的参数
    for last; do true; done

    mkdir $last -p
    \cp -rfLv $@
}

# 强制移动(哪怕目录不在)
function mvmv ()
{
	if [ $# -le 1 ]; then
        return
	fi
    # 获取最后的参数
    for last; do true; done

    if [ ! -d "$last" ];then
        mkdir $last -p
    fi
    mv $@ -vf
}

# 获取目录最深的层数
function dir-get-depth-max() {
    folder_name="$1"

    if [ ! -d "$folder_name" ];then
        folder_name=.
    fi

    this_dir_tmp=`pwd`

    folder_name=`echo "$folder_name"|sed "s#^./#$this_dir_tmp/#g"`
    folder_name=`echo "$folder_name"|sed "s#^\([a-zA-Z]\+.*\)#$this_dir_tmp/\1#g"`

    depth_foler()
    {
        this_dir=`pwd`
        source_folder="$1"
        source_folder=`echo $source_folder |sed 's#/$##g'`
        test_folder="$2"

        cd $test_folder
        count=0
        while [ ! `pwd` = $source_folder ]
        do
            count=`expr $count + 1`
            cd ..
        done

        cd $this_dir
        return $count
    }

    target_folder="$folder_name"
    depth_max=1

    for i in `du "$target_folder"` ;do
        if [ -d $i -a ! $i = $target_folder ];then
            depth_foler "$target_folder" "$i"
            retval=$?
            if [ $depth_max -lt $retval ];then
                    depth_max=$retval
            fi
        fi

    done

    echo "max depth: $depth_max"
}

# 获取 绝对路径
function dir-get-path-ab () {
    local path=$1
    local cur=$2 # optional
    local tmp=""
	local path_o=""
    local path_rm_flag=0

	if [ -z "$cur" ]; then
		cur=`pwd`
	fi

    if [ ! -d "${cur}" ]; then
		echo not-found
        return 1
    fi

    if [ ! -d "${path}" ]; then
        ## 创建上层目录
        mkdir -vp `dirname $path`
        path_rm_flag=1
    fi
    echo $1

    tmp=$(dirname $path)
    echo $tmp
    tmp=`cd $cur;cd $tmp; pwd`
    echo $tmp

	if [ $path_rm_flag -eq 1 ]; then
        rm -r `dirname $path` 2>/dev/null
	fi
	echo $tmp/`basename $path`
}

# 批量修改文件后缀
function resuffix () {
function resuffix_usage () {
    local funname=$1
    local err_state=$2
cat << EOF
usage :
  $funname dir old_suffix  new_suffix

 - 1. dir : From where to re-suffix

 - 2. old_suffix : Old file suffix

 - 3. new_suffix : New file suffix

e.g. :
  $funname  . jpeg  png

EOF

    if [  $err_state -eq 1 ]; then
        echo "-------"
        echo "error : Not dir."
        return 1
	fi
    if [  $err_state -eq 2 ]; then
        echo "-------"
        echo "error : suffix empty."
        return 1
	fi
}
    local from=$1
    local old_suffix=$2
    local new_suffix=$3
    local file_list=/tmp/change_suff
    if [ -z "$from" ]; then
        resuffix_usage $0 1
		return 1
	fi
    if [ -z "$old_suffix" ]; then
        resuffix_usage $0 2
		return 1
	fi
    if [ -z "$new_suffix" ]; then
        resuffix_usage $0 2
		return 1
	fi

    find $from -type f -name "*.${old_suffix}" > ${file_list}
    for i in `cat ${file_list}`;do
        mv  -v $i ${i/.${old_suffix}/.${new_suffix}}
    done 2> /dev/null
    rm ${file_list}
}

## 合并2个目录
function dir-merge-to () {
function dir-merge-to-help () {
cat <<EOF
$0 : merge 2 dirs
arg1: source dir
arg2: dest   dir

-----
e.g. :
   mkdir -vp a/same b/same a/adir/ b/bdir
   touch a/same/sa1 b/same/sa2 a/adir/a b/bdir/b
   tree a b
   dir-merge-to a b
   tree b
EOF
}

    local dir_source=$1
    local dir_dest=$2

    if [ ! -d "$dir_source" ]; then
		dir-merge-to-help $0; return 1
	fi
    if [ ! -d "$dir_dest" ]; then
		dir-merge-to-help $0; return 1
	fi
    cp --force --archive --update --link $dir_source/. $dir_dest
}

## 快速统计目录大小
function dir-usage () {
    # 目录层级
    local depth=$1

    if [  -z "$depth" ]; then
		depth=1
	fi
	du  --max-depth $depth -h  | sort -h
}
function duu () {
    dir-usage "$1"
}
