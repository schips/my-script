# --------------------------------------------------------------------------- #
# 获取文件名后缀
# Parameter1: 文件名
# output: Yes
# return: None
# --------------------------------------------------------------------------- #
function file-name-get-suffix() {
    local filename="$1"
    local ret
    if [ -z "$filename" ]; then
        return
    fi
    # 考虑没有后缀的文件，例如Makefile
    local basenm=`basename $filename`
    ret=`echo $basenm | grep '\.'`
    if [ -z "$ret" ]; then
        echo $basenm
        return
    fi
    if [ -n "$filename" ]; then
        echo "${filename##*.}"
    fi
}

# --------------------------------------------------------------------------- #
# 获取文件名前缀
# Parameter1: 文件名
# output: Yes
# return: None
# --------------------------------------------------------------------------- #
function file-name-get-prefix() {
    local filename="$1"
    if [ -n "$filename" ]; then
        echo "${filename%.*}"
    fi
}



# 替换文件的后缀名(允许提供目录)
function file-name-change-suffix() {
function _do_file_change_suffix() {
    local filename="$1"
    local suffix_old="$2"
    local suffix_new="$3"
    if [ -z "$suffix_old" ];then
        cat <<EOF
e.g :
  file-name-change-suffix   a.c    cpp
  file-name-change-suffix   a.c  c cpp
  file-name-change-suffix  ./ c    cpp
EOF
        return 7
    fi
    if [ "$(file-name-get-suffix ${filename})" = "$suffix_old" ]; then
        prefix="$(file-name-get-prefix ${filename})"
        mv -v ${filename} ${prefix}.${suffix_new}
        return $?
    fi
}
    local filename="$1"
    local suffix_old="$2"
    local suffix_new="$3"
    #判断file的类型，是文件还是目录
    ## 文件：判断目录并执行对应的命令
    if [ -f "${filename}" ]; then
        if [ $# -eq 2 ];then
            suffix_new=$suffix_old
            suffix_old="$(file-name-get-suffix ${filename})"
            _do_file_change_suffix "${filename}" "${suffix_old}" "$suffix_new"
        fi
        if [ $# -eq 3 ];then
            _do_file_change_suffix "${filename}" "${suffix_old}" "$suffix_new"
        fi
        return $?
    elif [  -d "${filename}" ]; then
        find . -name "*.${suffix_old}" | while read f
        do
            _do_file_change_suffix "${f}" "${suffix_old}" "$suffix_new"
        done

        return $?
    else
        return 8
    fi
    return 8
}
# --------------------------------------------------------------------------- #
# 判断文件后缀是否是指定后缀
# Parameter1: 文件名
# parameter2: 后缀名
# output: None
# return: 0: 表示文件后缀是指定后缀；1: 表示文件后缀不是指定后缀
# --------------------------------------------------------------------------- #
function file-name-is-suffix() {
    local filename="$1"
    local suffix="$2"

    if [ -z "$suffix" ]; then
        return 1
    fi
    if [ -z "$filename" ]; then
        return 1
    fi

    if [ "$(file-get-suffix ${filename})" = "$suffix" ]; then
        return 0
    else
        return 1
    fi
}

# --------------------------------------------------------------------------- #
# 如果文件以xx结尾，那么执行对应的动作
# Parameter1: 文件名
# parameter2: 后缀名
# parameter3: cmd命令，会以满足的文件名作为最后的参数
# output: None
# return: 8: 错误参数或不满足执行条件
# --------------------------------------------------------------------------- #
function file-name-do-cmd-if-suffix() {
    local filename="$1"
    local suffix="$2"
    local action="$3"

    if [ -z "$suffix" ]; then
        echo "file-name-do-cmd-if-suffix /tmp/ txt  ls"
        return 8
    fi
    if [ -z "$filename" ]; then
        echo "file-name-do-cmd-if-suffix /tmp/ txt  ls"
        return 8
    fi
    if [ -z "$action" ]; then
        echo "file-name-do-cmd-if-suffix /tmp/ txt  ls"
        echo "file-name-do-cmd-if-suffix /tmp/ txt  \"chmod -x\""
        return 8
    fi

    #判断file的类型，是文件还是目录
    ## 文件：判断目录并执行对应的命令
    if [ -f "${filename}" ]; then
        if [ "$(file-name-get-suffix ${filename})" = "$suffix" ]; then
            ${action} "${filename}"
            return $?
        fi
    fi

    if [  -d "${filename}" ]; then
#       echo "find . -name \"*.${suffix}\" | xargs -i ${action} {}"
#       find . -name "*.${suffix}" | xargs -i ${action} {}
        local cmd_list=/tmp/.$$.$USER.cmdlist
        rm -rf $cmd_list ; touch $cmd_list
        find . -name "*.${suffix}" | while read f
        do
            echo "${action} \"$f\"" >> $cmd_list
        done
        source $cmd_list
        rm $cmd_list
    fi

    return 8
}

