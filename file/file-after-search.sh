#*  @brief        基于文件清单，并做一些事情
#!/bin/sh

# 根据提供的find 结果，把文件移植到外面(保留目录结构)
function file-cp-transplant ()
{
function tran_help()
{
    cat <<EOF
file-cp-transplant find-list cp-output-path
e.g.:
    $ find . | grep Readme
        ./buildroot/package/rockchip/pm-suspend-api/src/Readme
        ./tools/linux/Firmware_Merger/Readme.txt
    $ find . | grep Readme >   /tmp/list
    $ file-cp-transplant  /tmp/list /tmp/test-file-cp-transplant/
    $ tree -aif /tmp/test-file-cp-transplant/
EOF
}
    local FILE_LIST="$1"
    local output_path="$2"
    if [ ! -f  $FILE_LIST ]; then
        tran_help
        return 1
    fi
    if [ -z  $output_path ]; then
        tran_help
        return 1
    fi
    if [ ! -d  $output_path ]; then
        mkdir -p $output_path
    fi
    local tran_path=""
    cat $FILE_LIST | while read line
    do
        if [ ! -f  $line ]; then
            echo "$line in $FILE_LIST but not found!"
            return 1
        fi
        tran_path=$output_path/`dirname $line`
        mkdir -p $tran_path
        cp -rfv $line $tran_path
    done
}
