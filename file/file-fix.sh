#*  @brief        修复文件
#!/bin/sh

# fix : corrupt history file /home/$USER/.zsh_history
function file-fix-zsh-history ()
{
    if [ -f  $HOME/.zsh_history ]; then
        mv -f $HOME/.zsh_history $HOME/.zsh_history_bad
        strings $HOME/.zsh_history_bad > $HOME/.zsh_history
        fc -R $HOME/.zsh_history
    fi
}
