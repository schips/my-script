#*  @brief        搜索有关的

#!/bin/sh

# find text in current path
# e.g. : ft main
function ft() {
    if [ $# -eq 0 ]; then
        return 1
    fi
	#local FILE_LIST=/tmp/.ft.list.$$
	## 限制每行的输出长度为200
    grep -nR 2>/dev/null "$*" | while read line
	do
		echo $line | cut -c 1-200 | grep "$*"
	done
}
function find-text() {
    ft "$@"
}

# find text in current path and replace
# e.g. : ft main
function ft-and-replace () {
    local from="$1"
    local to="$2"

    if [ $# -eq 0 ]; then
        echo "find 'abc' then replace with 'xyz'"
        echo "  oldstr  newstr"
        return 1
    fi

    local sfile=""
    grep -nR 2>/dev/null -m 1 "$from"| awk  -F ':' '{print $1}'  | uniq | sort | while read sfile
    do
        #echo "file-replace-string $sfile $from $to"
        file-replace-string $sfile "$from" "$to"
    done
}
function find-text-and-replace() {
    ft-and-replace "$@"
}

# fine word in current path
# e.g. : fw main
function fw() {
    if [ $# -eq 0 ]; then
        return 1
    fi
	## 限制每行的输出长度为200
    grep -w -nR 2>/dev/null "$*" | while read line
	do
		echo $line | cut -c 1-200 | grep "$*"
	done
}
function find-word() {
    ft "$@"
}

# --------------------------------------------------------------------------- #
# 如果文件以xx结尾，那么搜索其内容是否包含指定文本
# parameter1~(N-1): 后缀名
# parameterN(最后一个): 搜索内容
# return: 8: 错误参数或不满足执行条件
# --------------------------------------------------------------------------- #
function find-text-if-suffix() {
    local suffix="$1"
    local text="$2"

    if [ $# -eq 0 ]; then
        echo "find-text-if-suffix txt  hello"
        return 8
    fi

    # 分离 前N个 参数，和最后一个参数
    suffix=""
    until [ $# -eq 1 ]
    do
        suffix=`echo $suffix $1`
        shift
    done
    text="$1"
    for ss in `echo $suffix`
    do
        # 过滤掉 参数是 .c 的情况
        s="$(file-name-get-suffix ${ss})"
        echo "in <.$s> file, search $text"
        find -name "*.${s}" | xargs grep -nr "$text" | grep "$text"
        echo ""
    done
    return 0
}

# --------------------------------------------------------------------------- #
# 如果文件以xx结尾，那么搜索其内容是否包含指定单词
# parameter1~(N-1): 后缀名
# parameterN(最后一个): 搜索内容
# return: 8: 错误参数或不满足执行条件
# --------------------------------------------------------------------------- #
function find-word-if-suffix() {
    local suffix="$1"
    local text="$2"

    if [ $# -eq 0 ]; then
        echo "find-word-if-suffix txt  hello"
        return 8
    fi

    # 分离 前N个 参数，和最后一个参数
    suffix=""
    until [ $# -eq 1 ]
    do
        suffix=`echo $suffix $1`
        shift
    done
    text="$1"

    for ss in `echo $suffix`
    do
        # 过滤掉 参数是 .c 的情况
        s="$(file-name-get-suffix ${ss})"
        echo "in <.$s> file, search $text"
        find -name "*.${s}" | xargs grep -w -nr "$text" | grep "$text"
        echo ""
    done
    return 0
}

# find text in current path uniq
# 根据关键词筛选当前目录下对应的文件
# e.g. :  ft1 main
function ft1() {
	## 保存只有文件名的搜索结果
    if [ $# -eq 0 ]; then
        echo "ftx string ..."
        return 1
    fi

    if [ $# -eq 1 ]; then
        grep -nR 2>/dev/null -m 1 "$@"| awk  -F ':' '{print $1}'  | uniq | sort
    else
        # 如果有多个参数，那么进行筛选，找出同时存在的搜索结果
        local file_id=/tmp/$$.list
        echo "" > $file_id
        while [[ $# -gt 0 ]]
        do
            local match="$1"
            grep -nR 2>/dev/null -m 1 "$match"| awk  -F ':' '{print $1}'  | uniq | sort  >>  $file_id
            shift
        done
        cat $file_id | uniq -d
    fi
}
function find-file-about-text-uniq() {
    ft1 "$@"
}

# find path contained xx
# e.g. :  ff kernel
alias ff="find . 2>/dev/null | grep"
# --------------------------------------------------------------------------- #
# 搜索指定类型的文件
# parameter1: 后缀名
# parameter2: 文件名
# return: 8: 错误参数或不满足执行条件
# --------------------------------------------------------------------------- #
function find-file-if-suffix() {
    local suffix="$1"
    local file_name="$2"

    if [ $# -eq 0 ]; then
        echo "find-file-if-suffix txt  readme"
        return 8
    fi

    local s=""
    local raw_ret=""
    local grep_ret=""
    # 过滤掉 参数是 .c 的情况
    s="$(file-name-get-suffix ${suffix})"

    # 考虑后缀名匹配，但是文件名不匹配的情况
    #find -name "*.${s}" | grep "$file_name"
    raw_ret=`find -name "*.${s}" | grep "$file_name"`
    for f in `echo $raw_ret`
    do
        file_base=`basename $f`
        grep_ret=`echo $file_base | grep $file_name`
        if [ ! -z $grep_ret ]; then
            echo $f | grep $file_name
        fi
    done
}

# find dir
# e.g. :  ffdir  src
function ffdir()
{
    find . -type d -name "$1" 2> /dev/null | grep $1
}
function find-dir() {
    ffdir "$@"
}

# find all dir 's name contained xx
# e.g. :  ffadir  src
function ffadir()
{
    find . -type d -name "*$1*" 2> /dev/null | grep $1
}
function find-dir-any() {
    ffadir "$@"
}

# find regular file named xx only
# e.g. :  ffreg  main.c
function ffonly ()
{
    find . -type f -name "$1" 2> /dev/null | grep $1
}
function find-name-only() {
    ffonly "$@"
}

# find regular file 's name contained xx
# e.g. :  ffareg  main.c
function ffareg ()
{
    find . -type f -name "*$1*" 2> /dev/null | grep $1
}
function find-name-any() {
    ffareg "$@"
}

# 打印匹配搜索结果的文件全部内容
# dump find text in file found with path
function dft1 ()
{
    local files=`grep -nR 2>/dev/null -m 1 $@| awk  -F ':' '{print $1}'  | uniq | sort`
    if [ -z "$files" ]; then
        return 1
    fi
    echo $files | while read line
    do
        echo ""
        echo $line
        if [ -z "$line" ]; then
            continue
        fi
        cat $line
    done
}
function find-name-and-dump-all-if-match-string() {
    dft1 "$@"
}
