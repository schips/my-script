#!/bin/sh

## 在/tmp目录下生成随机文件
function tmp-gen-safe-file () {
    local cur_date=`cur-date-pure`
    local ur=$USER
    local pp=$$

    echo "/tmp/.$pp.$ur.$cur_date.$RANDOM"
}
