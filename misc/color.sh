
function echoRed ()
{
    echo -e "\033[31m" "$@" "\033[0m"
}

function echoBlack ()
{
    echo -e "\033[30m" "$@" "\033[0m"
}

function echoBlueDim ()
{
    echo -e "\033[36m" "$@" "\033[0m"
}

function echoWhite ()
{
    echo -e "\033[37m" "$@" "\033[0m"
}

function echoPepo ()
{
    echo -e "\033[35m" "$@" "\033[0m"
}

function echoBlue ()
{
    echo -e "\033[34m" "$@" "\033[0m"
}

function echoYellow ()
{
    echo -e "\033[33m" "$@" "\033[0m"
}

function echoGreen ()
{
    echo -e "\033[32m" "$@" "\033[0m"
}

function echoSetNormal ()
{
    echo -e -n "\033[0m"
}

function echoInLineRed ()
{
    echo -e -n "\033[31m" "$@" "\033[0m"
}

function echoInLineBlack ()
{
    echo -e -n "\033[30m" "$@" "\033[0m"
}

function echoInLineBlueDim ()
{
    echo -e -n "\033[36m" "$@" "\033[0m"
}

function echoInLineWhite ()
{
    echo -e -n "\033[37m" "$@" "\033[0m"
}

function echoInLinePepo ()
{
    echo -e -n "\033[35m" "$@" "\033[0m"
}

function echoInLineBlue ()
{
    echo -e -n "\033[34m" "$@" "\033[0m"
}

function echoInLineYellow ()
{
    echo -e -n "\033[33m" "$@" "\033[0m"
}

function echoInLineGreen ()
{
    echo -e -n "\033[32m" "$@" "\033[0m"
}
