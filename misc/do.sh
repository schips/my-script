#!/bin/sh
#*  @brief        便捷操作

##############################################
# 自己写的一个 命令流
#
# 用于处理 类似 cmd A/xx.c B/xx.c 这样 的命令

# Command
C=""
# Arg1
A1=""
# Arg2
A2=""

# 以val作为连接参数，执行command arg1-val arg2-val
function caa-do ()
{
    local VAL=$1
    if [ -z "$VAL" ]; then
        return
    fi
    local tmpcmd=`tmp-gen-safe-file`
    cat > $tmpcmd <<EOF
echo '$C ${A1}${VAL} ${A2}${VAL}'
      $C ${A1}${VAL} ${A2}${VAL}
EOF
    source $tmpcmd
    rm $tmpcmd
}

function caa-do-with-list ()
{
    # file list
    local filelist=$1
    #
    local enable_inquire=$2
    local i=0

    if [ ! -f "$filelist" ]; then
        return
    fi
    local donefile=`tmp-gen-safe-file`
    file-make-empty $donefile
    for VAL in `cat $filelist`
    do
        i=$(($i+1)) # 自增
        echo "[$i] $VAL"
        echo $VAL >> $donefile
        d12 $VAL
        if [ -z "$vv"  ]; then
            continue
        fi
        #if [ $? -ne 0  ]; then
        echo -e  "Continue? :\n\ts  : Stop\n\t(defalut : continue)"
        read answer
        case $answer in
            n ) continue;;
            s ) break;;
            k ) echo "Not Support now"; continue;;
            * ) continue;;
        esac
        #fi
    done
    rm $tmpfile
}

# SET COMMAMD ARG ARG
function caa-set()
{
    C="$1"
    A1="$2"
    A2="$3"
}

function caa-help ()
{
cat<<EOF
Usage:
   用于处理 类似 cmd A/xx.c B/xx.c 这样 的命令

    场景引入：
       vimdiff  Android10_R01_r012/Makefile Android11_R01_r08/Makefile
        ...

    可以使用下面的命令代替：
        # 指定 命令以及2个路径
        caa-set vimdiff  Android10_R01_r012/  Android11_R01_r08/
        # 此后，直接输入 相对路径下的文件名
        caa-do Makefile
        ...
EOF
}
