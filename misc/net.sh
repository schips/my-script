#!/bin/sh

## 检查端口被什么进程给占用
function net-check-port () {
    local port=$1
    if [  -z "$1" ]; then
        echo "nedd port"
        return 1
    fi
    local pids=`lsof -i:${port} | grep -v "COMMAND" | grep -v "PID" | awk '{print$2}'`
    #echo $pids
    for pid in $pids;
    do
        ps -aux | grep -v grep | grep -E "$pid|$port"
    done
    echo "For more detail, use 'check-port-server'"
}

## 检查服务使用什么端口
function net-check-port-server () {
    local port=$1
    if [  -z "$1" ]; then
        echo "nedd port"
        return 1
    fi
    sudo netstat -ltupn | grep ${port}
}
