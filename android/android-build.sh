#!/bin/bash
## bengal, msm8909, sdm845 ...
export ALUNCH_ITEM=""
## user, userdebug
export ALUNCH_MODE=""
## boot/dtb/kernel/all
export AMAKE_TARGET=""
## IMAGE OUPUT PATH
export AMAKE_OUTPUT=""


function android-sets()
{
    if [ -z "$1" ]; then
        echo "e.g. : msm8909 userdebug kernel"
        return 1
    fi
    android-set-itemmode $1 $2
    android-set-target $3
}

function android-set-itemmode()
{
    ALUNCH_ITEM=$1
    ALUNCH_MODE=$2

    if [ -z "$ALUNCH_ITEM" ]; then
        echo "e.g. : msm8909 userdebug"
        return 1
    fi

    if [ "$ALUNCH_MODE" = "u" -o "$ALUNCH_MODE" = "user" ]; then
        ALUNCH_MODE="user"
    else
        ALUNCH_MODE="userdebug"
    fi
    echo "Set lunch item is [$ALUNCH_ITEM] with [$ALUNCH_MODE]"
}

function android-set-target()
{
    case $1 in
        a | all )
            AMAKE_TARGET="all"
            ;;
        k | kernel )
            AMAKE_TARGET="kernel"
            ;;
        d | dtbo )
            AMAKE_TARGET="dtb"
            ;;
        b | boot )
            AMAKE_TARGET="boot"
            ;;
        * )
            echo "unknow item"
            return 1
            ;;
    esac
    echo "Set Make [$AMAKE_TARGET]"
}

function android-set-output()
{
    AMAKE_OUTPUT="$1"
    mkdir -vp $AMAKE_OUTPUT
    echo "Set Output [$AMAKE_OUTPUT]"
}

function android-build () {

    ## thread
    local AMAKE_THREAD=$1

    local here=`pwd`

    if [ -z "$ALUNCH_ITEM" ]; then
        echo "Use [android-set-itemmode] first."
        return 1
    fi
    if [ -z "$ALUNCH_MODE" ]; then
        echo "Use [android-set-itemmode] first."
        return 1
    fi
    if [ -z "$AMAKE_TARGET" ]; then
        echo "Use [android-set-target] first."
        return 1
    fi
    if [ -z "$AMAKE_OUTPUT" ]; then
        echo "Use [android-set-output] first."
        return 1
    fi

    if [ -z "$AMAKE_THREAD" ]; then
		AMAKE_THREAD="-j20"
        echo "Build with [$AMAKE_THREAD]"
    fi

    local MOUT_TMP_CMD="my.build.bash"
    rm  -rf $MOUT_TMP_CMD > /dev/null 2>&1
    #git-root
    # gen make command
(
cat <<EOF
#!/bin/bash
ARRAY_ALL_ITEM=(msm8909 msm8937_32 sdm845 bengal)

# msm8909
declare -A AOBJMAP_MSM8909
AOBJMAP_MSM8909["before"]="export LC_ALL=C"
AOBJMAP_MSM8909["boot"]="make aboot"
AOBJMAP_MSM8909["dtb"]="make bootimage"
AOBJMAP_MSM8909["kernel"]="make bootimage"
AOBJMAP_MSM8909["all"]="make "
ARRAY_MSM8909_ALL=(system.img persist.img recovery.img userdata.img cache.img)
ARRAY_MSM8909_KER=(boot.img)
ARRAY_MSM8909_DTB=()
ARRAY_MSM8909_BOT=(emmc_appsboot.mbn)

# msm8937_32
declare -A AOBJMAP_MSM8937_32
AOBJMAP_MSM8937_32["before"]=""
AOBJMAP_MSM8937_32["boot"]="make aboot"
AOBJMAP_MSM8937_32["dtb"]="make dtboimage"
AOBJMAP_MSM8937_32["kernel"]="make bootimage"
AOBJMAP_MSM8937_32["all"]="make "
ARRAY_MSM8937_32_ALL=(system.img vendor.img persist.img recovery.img userdata.img)
ARRAY_MSM8937_32_KER=(boot.img)
ARRAY_MSM8937_32_DTB=(dtbo.img)
ARRAY_MSM8937_32_BOT=(emmc_appsboot.mbn)

# sdm845
declare -A AOBJMAP_SDM845
AOBJMAP_SDM845["before"]=" "
AOBJMAP_SDM845["boot"]="make aboot"
AOBJMAP_SDM845["dtb"]="make dtboimage"
AOBJMAP_SDM845["kernel"]="make bootimage"
AOBJMAP_SDM845["all"]="make "
ARRAY_SDM845_ALL=(system.img vendor.img persist.img userdata.img vbmeta.img)
ARRAY_SDM845_KER=(boot.img)
ARRAY_SDM845_DTB=(dtbo.img)
ARRAY_SDM845_BOT=(abl.elf)

# bengal
declare -A AOBJMAP_SM6115
AOBJMAP_SM6115["before"]=" "
AOBJMAP_SM6115["boot"]="./build.sh aboot"
AOBJMAP_SM6115["dtb"]="./build.sh dtboimage"
AOBJMAP_SM6115["kernel"]="./build.sh bootimage"
AOBJMAP_SM6115["all"]="./build.sh dist"
ARRAY_SM6115_ALL=(super.img persist.img metadata.img userdata.img vbmeta.img cache.img recovery.img)
ARRAY_SM6115_KER=(boot.img)
ARRAY_SM6115_DTB=(dtbo.img)
ARRAY_SM6115_BOT=(abl.elf)

# sm4350
declare -A AOBJMAP_SM4350
#AOBJMAP_SM4350["before"]=" "
#AOBJMAP_SM4350["boot"]="make aboot"
#AOBJMAP_SM4350["dtb"]="make dtboimage"
#AOBJMAP_SM4350["kernel"]="make bootimage"
#AOBJMAP_SM4350["system"]="./build.sh dist"
#ARRAY_SM4350_ALL=()
#ARRAY_SM4350_KER=()
#ARRAY_SM4350_DTB=()
#ARRAY_SM4350_BOT=()

ALUNCH_ITEM=""
AMAKE_TARGET=""

ARRAY_NULL=()
ARRAY_CUR_ALL=("\${ARRAY_NULL[@]}")
ARRAY_CUR_KER=("\${ARRAY_NULL[@]}")
ARRAY_CUR_DTB=("\${ARRAY_NULL[@]}")
ARRAY_CUR_BOT=("\${ARRAY_NULL[@]}")

BEFORE_MAKE=""
MAKE_BOOT__=""
MAKE_DTS___=""
MAKE_KERNEL=""
MAKE_ALL___=""

function setcmd() {
    ALUNCH_ITEM=\$1
    case \$ALUNCH_ITEM in
        msm8909 )
            BEFORE_MAKE="\${AOBJMAP_MSM8909['before']}"
            MAKE_BOOT__="\${AOBJMAP_MSM8909['boot']}  "
            MAKE_DTS___="\${AOBJMAP_MSM8909['kernel']}"
            MAKE_KERNEL="\${AOBJMAP_MSM8909['dtb']}   "
            MAKE_ALL___="\${AOBJMAP_MSM8909['all']}   "
            ;;
        msm8937_32 )
            BEFORE_MAKE="\${AOBJMAP_MSM8937_32['before']}"
            MAKE_BOOT__="\${AOBJMAP_MSM8937_32['boot']}  "
            MAKE_DTS___="\${AOBJMAP_MSM8937_32['kernel']}"
            MAKE_KERNEL="\${AOBJMAP_MSM8937_32['dtb']}   "
            MAKE_ALL___="\${AOBJMAP_MSM8937_32['all']}   "
            ;;
        sdm845 )
            BEFORE_MAKE="\${AOBJMAP_SDM845['before']}"
            MAKE_BOOT__="\${AOBJMAP_SDM845['boot']}  "
            MAKE_DTS___="\${AOBJMAP_SDM845['kernel']}"
            MAKE_KERNEL="\${AOBJMAP_SDM845['dtb']}   "
            MAKE_ALL___="\${AOBJMAP_SDM845['all']}   "
            ;;
        bengal )
            BEFORE_MAKE="\${AOBJMAP_SM6115['before']}"
            MAKE_BOOT__="\${AOBJMAP_SM6115['boot']}  "
            MAKE_DTS___="\${AOBJMAP_SM6115['kernel']}"
            MAKE_KERNEL="\${AOBJMAP_SM6115['dtb']}   "
            MAKE_ALL___="\${AOBJMAP_SM6115['all']}   "
            ;;
        * )
            echo "Warning, unknow product"
            return 1
            ;;
    esac
}

function mk() {
    local cmd=""
    AMAKE_TARGET=\$1
    case \$AMAKE_TARGET in
        boot )
            cmd="\$MAKE_BOOT__"
            ;;
        dtb )
            cmd="\$MAKE_DTS___"
            ;;
        kernel )
            cmd="\$MAKE_KERNEL"
            ;;
        all )
            cmd="\$MAKE_ALL___"
            ;;
        * )
            echo "unknow target"
            return 1
            ;;
    esac

    \$BEFORE_MAKE
    source build/envsetup.sh || return 1
    lunch ${ALUNCH_ITEM}-${ALUNCH_MODE}  || return 1
    \$cmd ${AMAKE_THREAD} 2>&1 | tee out/build.\${AMAKE_TARGET}.log
    makeret=\${PIPESTATUS[0]}
    if [ \$makeret -ne 0  ]
    then
        return 1
    fi
}

function cpoutput() {
    local gitRepoTOP=\`git rev-parse --show-toplevel\`
    local topDir=\`cd \$gitRepoTOP && pwd\`
    local here=\`pwd\`

    local MOUT="\$1"
    if [ -z "\$MOUT" ]; then
        local DATE=\$(date "+%Y%m%d%H%M%S")
        MOUT=~/tmp/\$DATE
    fi
    mkdir -vp \$MOUT
    local OUTPUT_IMAGE_PATH=\${gitRepoTOP}/out/target/product/\${ALUNCH_ITEM}/
    case \$ALUNCH_ITEM in
        msm8909 )
            ARRAY_CUR_ALL=("\${ARRAY_MSM8909_ALL[@]}")
            ARRAY_CUR_KER=("\${ARRAY_MSM8909_KER[@]}")
            ARRAY_CUR_DTB=("\${ARRAY_MSM8909_DTB[@]}")
            ARRAY_CUR_BOT=("\${ARRAY_MSM8909_BOT[@]}")
            ;;
        msm8937_32 )
            ARRAY_CUR_ALL=("\${ARRAY_MSM8937_32_ALL[@]}")
            ARRAY_CUR_KER=("\${ARRAY_MSM8937_32_KER[@]}")
            ARRAY_CUR_DTB=("\${ARRAY_MSM8937_32_DTB[@]}")
            ARRAY_CUR_BOT=("\${ARRAY_MSM8937_32_BOT[@]}")
            ;;
        sdm845 )
            ARRAY_CUR_ALL=("\${ARRAY_SDM845_ALL[@]}")
            ARRAY_CUR_KER=("\${ARRAY_SDM845_KER[@]}")
            ARRAY_CUR_DTB=("\${ARRAY_SDM845_DTB[@]}")
            ARRAY_CUR_BOT=("\${ARRAY_SDM845_BOT[@]}")
            ;;
        bengal )
            ARRAY_CUR_ALL=("\${ARRAY_SM6115_ALL[@]}")
            ARRAY_CUR_KER=("\${ARRAY_SM6115_KER[@]}")
            ARRAY_CUR_DTB=("\${ARRAY_SM6115_DTB[@]}")
            ARRAY_CUR_BOT=("\${ARRAY_SM6115_BOT[@]}")
            ;;
        * )
            echo "Warning, unknow product"
            return 1
            ;;
    esac
    case \$AMAKE_TARGET in
        all )
			for var in \${ARRAY_CUR_ALL[@]}
			do
			  cp -vf \${OUTPUT_IMAGE_PATH}/\$var \${MOUT}
			done
            ;&
        kernel )
			for var in \${ARRAY_CUR_KER[@]}
			do
			  cp -vf \${OUTPUT_IMAGE_PATH}/\$var \${MOUT}
			done
            ;&
        dtb )
			for var in \${ARRAY_CUR_DTB[@]}
			do
			  cp -vf \${OUTPUT_IMAGE_PATH}/\$var \${MOUT}
			done
            ;&
        boot )
			for var in \${ARRAY_CUR_BOT[@]}
			do
			  cp -vf \${OUTPUT_IMAGE_PATH}/\$var \${MOUT}
			done
            ;;
        * )
            echo "unknow item"
            return 1
            ;;
    esac
}

function run_script() {
    setcmd    ${ALUNCH_ITEM}
    mk        ${AMAKE_TARGET} || return 1
    cpoutput  ${AMAKE_OUTPUT}
}
run_script
EOF
) > $MOUT_TMP_CMD
    chmod +x  $MOUT_TMP_CMD
    bash $MOUT_TMP_CMD
    local ret=$?
cat <<EOF
Build [${ALUNCH_ITEM}] with [$ALUNCH_MODE]
Taget is [$AMAKE_TARGET] in [$AMAKE_OUTPUT]
EOF
    cd $here
    if [ $ret -ne 0 ]; then
        echo "Error when make"
        return 1
    fi
}
