#!/bin/sh
# 检查 系统版本
function errno ()
{
    function dump_errno_list()
    {
        cat <<EOF
Value	C Name	Description	含义
0	Success	Success	成功
1	EPERM	Operation not permitted	操作不允许
2	ENOENT	No such file or directory	没有这样的文件或目录
3	ESRCH	No such process	没有这样的过程
4	EINTR	Interrupted system call	系统调用被中断
5	EIO	I/O error	I/O错误
6	ENXIO	No such device or address	没有这样的设备或地址
7	E2BIG	Arg list too long	参数列表太长
8	ENOEXEC	Exec format error	执行格式错误
9	EBADF	Bad file number	坏的文件描述符
10	ECHILD	No child processes	没有子进程
11	EAGAIN	Try again	资源暂时不可用
12	ENOMEM	Out of memory	内存溢出
13	EACCES	Permission denied	拒绝许可
14	EFAULT	Bad address	错误的地址
15	ENOTBLK	Block device required	块设备请求
16	EBUSY	Device or resource busy	设备或资源忙
17	EEXIST	File exists	文件存在
18	EXDEV	Cross-device link	无效的交叉链接
19	ENODEV	No such device	设备不存在
20	ENOTDIR	Not a directory	不是一个目录
21	EISDIR	Is a directory	是一个目录
22	EINVAL	Invalid argument	无效的参数
23	ENFILE*	File table overflow	打开太多的文件系统
24	EMFILE	Too many open files	打开的文件过多
25	ENOTTY	Not a tty device	不是tty设备
26	ETXTBSY	Text file busy	文本文件忙
27	EFBIG	File too large	文件太大
28	ENOSPC	No space left on device	设备上没有空间
29	ESPIPE	Illegal seek	非法移位
30	EROFS	Read-only file system	只读文件系统
31	EMLINK	Too many links	太多的链接
32	EPIPE	Broken pipe	管道破裂
33	EDOM	Math argument out of domain	数值结果超出范围
34	ERANGE	Math result not representable	数值结果不具代表性
35	EDEADLK	Resource deadlock would occur	资源死锁错误
36	ENAMETOOLONG	Filename too long	文件名太长
37	ENOLCK	No record locks available	没有可用锁
38	ENOSYS	Function not implemented	功能没有实现
39	ENOTEMPTY	Directory not empty	目录不空
40	ELOOP	Too many symbolic links encountered	符号链接层次太多
41	EWOULDBLOCK	Same as EAGAIN	和EAGAIN一样
42	ENOMSG	No message of desired type	没有期望类型的消息
43	EIDRM	Identifier removed	标识符删除
44	ECHRNG	Channel number out of range	频道数目超出范围
45	EL2NSYNC	Level 2 not synchronized	2级不同步
46	EL3HLT	Level 3 halted	3级中断
47	EL3RST	Level 3 reset	3级复位
48	ELNRNG	Link number out of range	链接数超出范围
49	EUNATCH	Protocol driver not attached	协议驱动程序没有连接
50	ENOCSI	No CSI structure available	没有可用CSI结构
51	EL2HLT	Level 2 halted	2级中断
52	EBADE	Invalid exchange	无效的交换
53	EBADR	Invalid request descriptor	请求描述符无效
54	EXFULL	Exchange full	交换全
55	ENOANO	No anode	没有阳极
56	EBADRQC	Invalid request code	无效的请求代码
57	EBADSLT	Invalid slot	无效的槽
58	EDEADLOCK	Same as EDEADLK	和EDEADLK一样
59	EBFONT	Bad font file format	错误的字体文件格式
60	ENOSTR	Device not a stream	设备不是字符流
61	ENODATA	No data available	无可用数据
62	ETIME	Timer expired	计时器过期
63	ENOSR	Out of streams resources	流资源溢出
64	ENONET	Machine is not on the network	机器不上网
65	ENOPKG	Package not installed	没有安装软件包
66	EREMOTE	Object is remote	对象是远程的
67	ENOLINK	Link has been severed	联系被切断
68	EADV	Advertise error	广告的错误
69	ESRMNT	Srmount error	srmount错误
70	ECOMM	Communication error on send	发送时的通讯错误
71	EPROTO	Protocol error	协议错误
72	EMULTIHOP	Multihop attempted	多跳尝试
73	EDOTDOT	RFS specific error	RFS特定的错误
74	EBADMSG	Not a data message	非数据消息
75	EOVERFLOW	Value too large for defined data type	值太大,对于定义数据类型
76	ENOTUNIQ	Name not unique on network	名不是唯一的网络
77	EBADFD	File descriptor in bad state	文件描述符在坏状态
78	EREMCHG	Remote address changed	远程地址改变了
79	ELIBACC	Cannot access a needed shared library	无法访问必要的共享库
80	ELIBBAD	Accessing a corrupted shared library	访问损坏的共享库
81	ELIBSCN	A .lib section in an .out is corrupted	库段. out损坏
82	ELIBMAX	Linking in too many shared libraries	试图链接太多的共享库
83	ELIBEXEC	Cannot exec a shared library directly	不能直接执行一个共享库
84	EILSEQ	Illegal byte sequence	无效的或不完整的多字节或宽字符
85	ERESTART	Interrupted system call should be restarted	应该重新启动中断的系统调用
86	ESTRPIPE	Streams pipe error	流管错误
87	EUSERS	Too many users	用户太多
88	ENOTSOCK	Socket operation on non-socket	套接字操作在非套接字上
89	EDESTADDRREQ	Destination address required	需要目标地址
90	EMSGSIZE	Message too long	消息太长
91	EPROTOTYPE	Protocol wrong type for socket	socket协议类型错误
92	ENOPROTOOPT	Protocol not available	协议不可用
93	EPROTONOSUPPORT	Protocol not supported	不支持的协议
94	ESOCKTNOSUPPORT	Socket type not supported	套接字类型不受支持
95	EOPNOTSUPP	Operation not supported on transport	不支持的操作
96	EPFNOSUPPORT	Protocol family not supported	不支持的协议族
97	EAFNOSUPPORT	Address family not supported by protocol	协议不支持的地址
98	EADDRINUSE	Address already in use	地址已在使用
99	EADDRNOTAVAIL	Cannot assign requested address	无法分配请求的地址
100	ENETDOWN	Network is down	网络瘫痪
101	ENETUNREACH	Network is unreachable	网络不可达
102	ENETRESET	Network dropped	网络连接丢失
103	ECONNABORTED	Software caused connection	软件导致连接中断
104	ECONNRESET	Connection reset by	连接被重置
105	ENOBUFS	No buffer space available	没有可用的缓冲空间
106	EISCONN	Transport endpoint	传输端点已经连接
107	ENOTCONN	Transport endpoint	传输终点没有连接
108	ESHUTDOWN	Cannot send after transport	传输后无法发送
109	ETOOMANYREFS	Too many references	太多的参考
110	ETIMEDOUT	Connection timed	连接超时
111	ECONNREFUSED	Connection refused	拒绝连接
112	EHOSTDOWN	Host is down	主机已关闭
113	EHOSTUNREACH	No route to host	没有主机的路由
114	EALREADY	Operation already	已运行
115	EINPROGRESS	Operation now in	正在运行
116	ESTALE	Stale NFS file handle	陈旧的NFS文件句柄
117	EUCLEAN	Structure needs cleaning	结构需要清洗
118	ENOTNAM	Not a XENIX-named	不是XENIX命名的
119	ENAVAIL	No XENIX semaphores	没有XENIX信号量
120	EISNAM	Is a named type file	是一个命名的文件类型
121	EREMOTEIO	Remote I/O error	远程输入/输出错误
122	EDQUOT	Quota exceeded	超出磁盘配额
123	ENOMEDIUM	No medium found	没有磁盘被发现
124	EMEDIUMTYPE	Wrong medium type	错误的媒体类型
125	ECANCELED	Operation Canceled	取消操作
126	ENOKEY	Required key not available	所需键不可用
127	EKEYEXPIRED	Key has expired	关键已过期
128	EKEYREVOKED	Key has been revoked	关键被撤销
129	EKEYREJECTED	Key was rejected by service	关键被拒绝服务
130	EOWNERDEAD	Owner died	所有者死亡
131	ENOTRECOVERABLE	State not recoverable	状态不可恢复
132	ERFKILL	Operation not possible due to RF-kill	由于RF-kill而无法操作
EOF
    }
    local id="$1"
    if [ -z "$id" ];then
        echo "Need err-inforamation."
        return 0
    fi
    dump_errno_list | grep -w "$id" | head -n 1
}
