#!/bin/sh
# 检查 系统版本
function checkos ()
{
    local ThisOS=""
    local SubVersion=""
    if [ -n "$(grep 'Aliyun Linux release' /etc/issue)" -o -e /etc/redhat-release ]
    then
        ThisOS=CentOS
        if [ -n "$(grep ' 7\.' /etc/redhat-release)" ]; then 
            SubVersion=7
        fi
        if [ -n "$(grep ' 6\.' /etc/redhat-release)" -o -n "$(grep 'Aliyun Linux release6 15' /etc/issue)" ]; then
            SubVersion=6
        fi
        if [ -n "$(grep ' 5\.' /etc/redhat-release)" -o -n "$(grep 'Aliyun Linux release5' /etc/issue)" ]; then 
            SubVersion=5
        fi
    elif [ -n "$(grep 'Amazon Linux AMI release' /etc/issue)" -o -e /etc/system-release ]
    then
        ThisOS=CentOS
        SubVersion=6
    elif [ -n "$(grep 'bian' /etc/issue)" -o "$(lsb_release -is 2>/dev/null)" = "Debian" ]
    then
        ThisOS=Debian
        #[ ! -e "$(which lsb_release)" ] && { apt-get -y update; apt-get -y install lsb-release; clear; }
        #Debian_version=$(lsb_release -sr | awk -F. '{print $1}')
    elif [ -n "$(grep 'Deepin' /etc/issue)" -o "$(lsb_release -is 2>/dev/null)" = "Deepin" ]
    then
        ThisOS=Debian
        #[ ! -e "$(which lsb_release)" ] && { apt-get -y update; apt-get -y install lsb-release; clear; }
        #Debian_version=$(lsb_release -sr | awk -F. '{print $1}')
        # kali rolling
    elif [ -n "$(grep 'Kali GNU/Linux Rolling' /etc/issue)" -o "$(lsb_release -is 2>/dev/null)" = "Kali" ]
    then
        ThisOS=Debian
        #[ ! -e "$(which lsb_release)" ] && { apt-get -y update; apt-get -y install lsb-release; clear; }
        #if [ -n "$(grep 'VERSION="2016.*"' /etc/os-release)" ]; then
        #  Debian_version=8
        #else
        #  echo "${CFAILURE}Does not support this OS, Please contact the author! ${CEND}"
        #  kill -9 $$
        #fi
    elif [ -n "$(grep 'Ubuntu' /etc/issue)" -o "$(lsb_release -is 2>/dev/null)" = "Ubuntu" -o -n "$(grep 'Linux Mint' /etc/issue)" ]
    then
        ThisOS=Ubuntu
        #[ ! -e "$(which lsb_release)" ] && { apt-get -y update; apt-get -y install lsb-release; clear; }
        SubVersion=$(lsb_release -sr | awk -F. '{print $1}')
        [ -n "$(grep 'Linux Mint 18' /etc/issue)" ] && SubVersion=16
    elif [ -n "$(grep 'elementary' /etc/issue)" -o "$(lsb_release -is 2>/dev/null)" = 'elementary' ]
    then
        ThisOS=Ubuntu
        #[ ! -e "$(which lsb_release)" ] && { apt-get -y update; apt-get -y install lsb-release; clear; }
        SubVersion=16
    else
        echo "${CFAILURE}Does not support this OS, Please contact the author! ${CEND}"
        kill -9 $$
    fi
    echo $ThisOS
    echo $SubVersion
}
